package com.selerys.sms.services;


import com.selerys.sms.bean.cm.push.StatusDetail;
import com.selerys.sms.bean.cm.push.StatusNotification;
import com.selerys.sms.bean.cm.push.StatusPush;
import com.selerys.sms.repository.SmsMessageRepository;
import com.selerys.sms.services.impl.CmPushService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verifyNoInteractions;

@RunWith(MockitoJUnitRunner.class)
public class CmPushServiceTest {


    @Mock
    SmsMessageRepository messageRepository;


    @InjectMocks
    CmPushService cmPushService;


    /**
     * Test du savePushedStatus sur l'objet :
     * StatusPush{msg=StatusNotification{received='2023-04-21T07:01:56', to='0033630544329', reference='', status=StatusDetail{code=2, errorCode='', errorDescription='Delivered'}}}
     */
    @Test
    public void testSavePushedStatus() {

        StatusPush statusPush = new StatusPush();

        StatusNotification notification = new StatusNotification();
        notification.setReference("");
        notification.setTo("0033630544329");
        notification.setReceived("2023-04-21T07:01:56");

        StatusDetail detail = new StatusDetail();
        detail.setCode(2);
        detail.setErrorCode("");
        detail.setErrorDescription("Delivered");

        notification.setStatus(detail);

        statusPush.setMsg(notification);

        cmPushService.savePushedStatus(statusPush);

        //succès si on n'appelle pas messageRepository pour faire les actions derrières
        verifyNoInteractions(messageRepository);
    }

}