package com.selerys.sms.services;

import com.selerys.sms.bean.launch.sequence.LaunchDeclarationBuilder;
import com.selerys.sms.bean.launch.sequence.LaunchDeclarationMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class LaunchDeclarationBuilderTest {

    @Test
    public void testpattern(){
        LaunchDeclarationBuilder launchDeclarationBuilder = new LaunchDeclarationBuilder("i=13;s=920793_0;f=38082;t=1666012451;c=200");
        LaunchDeclarationMessage launchDeclarationMessage = launchDeclarationBuilder.build();
        assertEquals(Integer.valueOf(200), launchDeclarationMessage.getCode());
        assertEquals("13", launchDeclarationMessage.getTransactionId());

        LaunchDeclarationBuilder launchDeclarationBuilder2 = new LaunchDeclarationBuilder("i=14;s=920793_0;n=3;f=38082;t=1666017390;c=10");
        LaunchDeclarationMessage launchDeclarationMessage2 = launchDeclarationBuilder2.build();
        assertEquals(Integer.valueOf(10), launchDeclarationMessage2.getCode());
        assertEquals("14", launchDeclarationMessage2.getTransactionId());
        assertEquals(Integer.valueOf(3), launchDeclarationMessage2.getBalloon());
    }
}
