/*
package com.selerys.sms.services;

import com.selerys.sms.exception.SmsException;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.services.impl.EsendexService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SmsProviderServiceTest {

    @Mock
    EsendexService esendexService;

    @InjectMocks
    SmsProviderService smsProviderService;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        //   doNothing().when(esendexService).sendSmsAndGetToken(ArgumentMatchers.any());
        //when(esendexService.sendSmsAndGetToken(ArgumentMatchers.any())).thenReturn("token");
    }

    @Test(expected = SmsException.class)
    public void sendSmsOnNullPhone() throws SmsException {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("test");
        smsMessage.setPhone(null);
        smsProviderService.tryToSendSmsSendReturnToken(smsMessage);
    }


    @Test(expected = SmsException.class)
    public void sendSmsOn06000() throws SmsException {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("test");
        smsMessage.setPhone("+33600000000");
        smsProviderService.tryToSendSmsSendReturnToken(smsMessage);
    }

    @Test(expected = SmsException.class)
    public void sendSmsOn06000Bis() throws SmsException {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("test");
        smsMessage.setPhone("33600000000");
        smsProviderService.tryToSendSmsSendReturnToken(smsMessage);
    }

    @Test(expected = SmsException.class)
    public void sendSmsOnEmptyPhone() throws SmsException {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("test");
        smsMessage.setPhone("");
        smsProviderService.tryToSendSmsSendReturnToken(smsMessage);
    }

    @Test(expected = SmsException.class)
    public void sendSmsOnBlankPhone() throws SmsException {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("test");
        smsMessage.setPhone(" ");
        smsProviderService.tryToSendSmsSendReturnToken(smsMessage);
    }

   // @Test
//    public void sendSmsSuccess() throws SmsException {
//        SmsMessage smsMessage = new SmsMessage();
//        smsMessage.setMessage("test");
//        smsMessage.setPhone("0601020304");
//        final String result = smsProviderService.tryToSendSmsSendReturnToken(smsMessage);
//        Assert.assertEquals(result, "token");
//    }

}*/
