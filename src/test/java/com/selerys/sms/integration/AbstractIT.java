package com.selerys.sms.integration;



import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

import org.junit.BeforeClass;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;

public abstract class AbstractIT {
    protected static WireMockServer wireMockServer = new WireMockServer(new WireMockConfiguration().port(7077));

    private static final PostgreSQLContainer POSTGRES_SQL_CONTAINER;

    static {
        POSTGRES_SQL_CONTAINER = new PostgreSQLContainer<>(DockerImageName.parse("postgres:14.2-alpine"))
                .withDatabaseName("selerysPG");
        POSTGRES_SQL_CONTAINER.start();
    }

    //configuration des paramétres de base de données postgres en profile de test
    @DynamicPropertySource
    static void overrideTestProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", POSTGRES_SQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.username", POSTGRES_SQL_CONTAINER::getUsername);
        registry.add("spring.datasource.password", POSTGRES_SQL_CONTAINER::getPassword);
    }
    @BeforeClass
    public static void setup() throws JSONException {
        wireMockServer.start();
        WireMock.configureFor("localhost",7077);
        wireMockServer.stubFor(WireMock.post("/mock/send/voice")
                .withHeader("Content-Type", containing("application/json"))
                .willReturn(ok().withBody("votre numero sert d'astrainte ...")));
        JSONObject cmSmsJsonResponse = new JSONObject().put("messages", new JSONArray().put(new JSONObject()
                .put("to", "0755623232")
                .put("status", "SENT")
                .put("messageDetails", "message sent successfully")
                .put("messageErrorCode", null)));

        wireMockServer.stubFor(WireMock.post("/mock/send/sms")
                .withHeader("Content-Type", containing("application/json"))
                .willReturn(ok()
                        .withHeader("Content-Type","application/json")
                        .withBody(cmSmsJsonResponse.toString())));
    }
    public PostgreSQLContainer getPostgresSqlContainer() {
        return POSTGRES_SQL_CONTAINER;
    }

}
