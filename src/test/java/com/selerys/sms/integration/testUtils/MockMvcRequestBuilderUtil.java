package com.selerys.sms.integration.testUtils;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.selerys.sms.model.tables.records.SmsMessageRecord;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;


//to avoid redundancy i created this class which builds MockMvc requests and expects specific things
// based on the method params.
@Component
public class MockMvcRequestBuilderUtil {

    @Autowired
    private MockMvc mockMvc;

    public void performPost(String url, Object body, ResultMatcher expectedStatus) throws Exception {
        if(url == null || body == null || expectedStatus == null){
            throw new Exception("missing method param");
        }
        this.mockMvc.perform(MockMvcRequestBuilders.post(url).content(new ObjectMapper().writeValueAsString(body))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(expectedStatus)
                .andDo(print());


    }

    public void performPost(String url, Object body, ResultMatcher expectedStatus, Exception expectedException) throws Exception {
        if(url == null || body == null || expectedStatus == null || expectedException == null){
            throw new Exception("missing method param");
        }
        //when & then
        this.mockMvc.perform(MockMvcRequestBuilders.post(url).content(new ObjectMapper().writeValueAsString(body))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(expectedStatus)
                .andExpect(result -> Assertions.assertTrue(expectedException.getClass().equals(result.getResolvedException().getClass())))
                .andDo(print());
    }
}
