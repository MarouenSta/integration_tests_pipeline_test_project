package com.selerys.sms.integration.testUtils;


import com.selerys.sms.model.Tables;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DBAssertionUtil {

    private final DSLContext dslContext;

    @Autowired
    DBAssertionUtil(DSLContext dslContext){
        this.dslContext = dslContext;
    }

    public void assertTableIsEmpty(Table tableName){
        assert dslContext.fetch(tableName).isEmpty();
    }

    public void assertRowContent(Table tableName, String columnName, String expectedValue){
        dslContext.selectFrom(tableName).fetch(columnName).get(0).equals(expectedValue);
    }


    public void assertTableIsNotEmpty(Table tableName){
        assert !dslContext.fetch(tableName).isEmpty();
    }
}
