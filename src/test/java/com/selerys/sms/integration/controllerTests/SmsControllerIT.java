package com.selerys.sms.integration.controllerTests;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import com.selerys.sms.configuration.AppConfiguration;
import com.selerys.sms.exception.MissingOrInvalidFieldException;
import com.selerys.sms.integration.AbstractIT;
import com.selerys.sms.integration.testUtils.DBAssertionUtil;
import com.selerys.sms.integration.testUtils.MockMvcRequestBuilderUtil;
import com.selerys.sms.model.Tables;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import org.jooq.DSLContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.util.List;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Testcontainers
@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class SmsControllerIT extends AbstractIT {

    @Autowired
    DSLContext dslContext;

    boolean setupDone = false;
    //we dump the db before each test and restore it after each test
    @Before
    public void dbDump() throws IOException, InterruptedException {
        //before all tests, we have a message object in the db which is the "astreinte" message
        //it is sent during the application startup
        //we want to delete that before starting the tests

        if(!setupDone){
            dslContext.deleteFrom(Tables.SMS_MESSAGE).execute();
            setupDone = true;
        }
        //this command dumps the db in a file called backup in the tmp folder of the container
        getPostgresSqlContainer()
                .execInContainer("pg_dump", "-U" , "test" , "-w" , "--clean","-d", "selerysPG", "-f", "/tmp/backup");
    }

    @After
    public void dbRestore() throws IOException, InterruptedException {
        //this command restores the db from the backup file
        getPostgresSqlContainer()
                .execInContainer("psql","-U", "test", "-d", "selerysPG", "-f","/tmp/backup");
    }

    //we create a mock server handled by spring boot to redirect the correct request to the corresponding
    //controller method using mockMvc.
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private AppConfiguration appConfiguration;

    @Autowired
    MockMvcRequestBuilderUtil mockMvcRequestBuilderUtil;

    @Autowired
    DBAssertionUtil dbAssertionUtil;


    //cm sms tests
    @Test
    public void shouldSendSmsViaCMAndReturn200Ok() throws Exception {
        SmsMessage sms = new SmsMessage();
        sms.setMessage("test 1");
        sms.setType("FAke");
        sms.setPhone("+3195050462");

        this.mockMvcRequestBuilderUtil.performPost("/sms/send/cm/FR",sms,status().isOk());
        //verify that only one request was sent to the wiremock server during this test
        verify(exactly(1),postRequestedFor(urlEqualTo("/mock/send/sms")));
        //check the database
        dbAssertionUtil.assertTableIsNotEmpty(Tables.SMS_MESSAGE);
        dbAssertionUtil.assertRowContent(Tables.SMS_MESSAGE,"message","test 1");
    }


    @Test
    public void shouldSendSmsWithMissingPhoneNumberViaCmAndReturn400() throws Exception {
        SmsMessage sms = new SmsMessage();
        sms.setMessage("test 2");

        this.mockMvcRequestBuilderUtil.performPost("/sms/send/cm/FR",sms,status().isBadRequest(),new MissingOrInvalidFieldException("phone",String.class));
        //check the database
        dbAssertionUtil.assertTableIsNotEmpty(Tables.SMS_MESSAGE);
        dbAssertionUtil.assertRowContent(Tables.SMS_MESSAGE,"message","test 2");
    }


    @Test
    public void shouldSendEmptyMessageViaCmAndReturn400() throws Exception {
        SmsMessage sms = new SmsMessage();
        sms.setPhone("0033780985064");

        this.mockMvcRequestBuilderUtil.performPost("/sms/send/cm/FR",sms,status().isBadRequest(),new MissingOrInvalidFieldException("message",String.class));
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    @Test
    public void shouldSendInvalidPhoneNumberViaCmAndReturn4xx() throws Exception {
        SmsMessage sms = new SmsMessage();
        sms.setMessage("hello");
        sms.setPhone("");

        this.mockMvcRequestBuilderUtil.performPost("/sms/send/cm/FR",sms,status().isBadRequest(),new MissingOrInvalidFieldException("phone",String.class));
        dbAssertionUtil.assertTableIsNotEmpty(Tables.SMS_MESSAGE);
        dbAssertionUtil.assertRowContent(Tables.SMS_MESSAGE,"message","hello");
    }

    //support sms test
    @Test
    public void shouldSendSupportSMSViaCmAndReturn2xx() throws Exception {
        SmsMessage sms = new SmsMessage();
        sms.setMessage("@éé&4");
        sms.setPhone("0033665985485");

        this.mockMvcRequestBuilderUtil.performPost("/sms/send/support",sms,status().is2xxSuccessful());
        dbAssertionUtil.assertTableIsNotEmpty(Tables.SMS_MESSAGE);
        dbAssertionUtil.assertRowContent(Tables.SMS_MESSAGE,"message","@éé&4");
    }

    @Test
    public void shouldSendInvalidSupportSMSNumberViaCmAndReturn2xx() throws Exception {
        SmsMessage sms = new SmsMessage();
        sms.setPhone("+");
        sms.setMessage("test1Z234");
        this.mockMvcRequestBuilderUtil.performPost("/sms/send/support",sms,status().is2xxSuccessful());
        dbAssertionUtil.assertTableIsNotEmpty(Tables.SMS_MESSAGE);
        dbAssertionUtil.assertRowContent(Tables.SMS_MESSAGE,"message","test1Z234");
    }

    @Test
    public void shouldSendEmptySupportMessageContentAndThrow4xxMissingField() throws Exception {
        SmsMessage sms = new SmsMessage();
        mockMvcRequestBuilderUtil.performPost("/sms/send/support",sms, status().is4xxClientError(), new MissingOrInvalidFieldException("phone",String.class));
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }


    //statelessSupportMessage test
    @Test
    public void shouldSendStatelessSupportSmsMessageAndReturn2xx() throws Exception {
        SmsMessage sms = new SmsMessage();
        sms.setPhone("+071645808");
        sms.setMessage("test 1234566");
        this.mockMvcRequestBuilderUtil.performPost("/sms/send/support/stateless",sms,status().is2xxSuccessful());
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    @Test
    public void shouldSendEmptyStatelessSupportSmsMessageAndReturn2xx() throws Exception{
        SmsMessage sms = new SmsMessage();

        this.mockMvcRequestBuilderUtil.performPost("/sms/send/support/stateless",sms,status().is2xxSuccessful());
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    //get cm from phone Tests

    @Test
    public void shouldGetFRCmPhoneAndReturn2xx() throws Exception{
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sms/conf/cm/FR"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(result -> Assertions.assertTrue(
                        result.getResponse().getContentAsString().equals(appConfiguration.getCmFrom("FR"))))
                .andDo(print());
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    @Test
    public void shouldGetCmPhoneWithEmptyCountryAndReturn4xx() throws Exception{
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sms/conf/cm/"))
                .andExpect(status().is4xxClientError())
                .andDo(print());
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    @Test
    public void shouldGetCmPhoneByFakeCountryAndReturn2xxAndDefaultCMFrom() throws Exception{
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sms/conf/cm/ZZ"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(result -> Assertions.assertTrue(
                        result.getResponse().getContentAsString().equals(appConfiguration.getCmFrom("FR"))))
                .andDo(print());
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    @Test
    public void shouldGetCmPhoneByROAndReturn2xx() throws Exception{
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sms/conf/cm/RO"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(result -> Assertions.assertTrue(
                        result.getResponse().getContentAsString().equals(appConfiguration.getCmFrom("RO"))))
                .andDo(print());
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    @Test
    public void shouldGetCmPhoneByGRAndReturn2xx() throws Exception{
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sms/conf/cm/GR"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(result -> Assertions.assertTrue(
                        result.getResponse().getContentAsString().equals(appConfiguration.getCmFrom("GR"))))
                .andDo(print());
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    //send sms and return token test
    @Test
    public void shouldSendSmsAndReturn2xx() throws Exception{
        SmsMessage sms = new SmsMessage();
        sms.setMessage("hello");
        sms.setPhone("+3265525");

        this.mockMvcRequestBuilderUtil.performPost("/sms/send",sms,status().is2xxSuccessful());
        dbAssertionUtil.assertTableIsNotEmpty(Tables.SMS_MESSAGE);
        dbAssertionUtil.assertRowContent(Tables.SMS_MESSAGE,"message","hello");
    }

    @Test
    public void shouldSendSmsAndReturn4xxMissingFieldException() throws Exception{
        SmsMessage sms = new SmsMessage();

        this.mockMvcRequestBuilderUtil.performPost("/sms/send",sms,status().is4xxClientError(),new MissingOrInvalidFieldException("phone",String.class));
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

    //on call duty sms and vocal message tests

    @Test
    public void shouldSendOnCallDutySMSAndVoiceMessageAndReturn2xx() throws Exception{
        boolean rule = true;
        boolean alert = true;
        SmsMessage sms = new SmsMessage();

        this.mockMvcRequestBuilderUtil.performPost("/sms/send/oncallduty?rule="+rule+"&alert="+alert,sms,status().is2xxSuccessful());
        dbAssertionUtil.assertTableIsNotEmpty(Tables.SMS_MESSAGE);
    }


    @Test
    public void shouldSendOnCallDutySmsAndVoiceMessageWithInvalidParamsAndReturn500() throws Exception{
        boolean rule = true;
        String alert = "string";
        SmsMessage sms = new SmsMessage();

        this.mockMvcRequestBuilderUtil.performPost("/sms/send/oncallduty?rule="+rule+"&alert="+alert,sms,status().is5xxServerError());
        dbAssertionUtil.assertTableIsEmpty(Tables.SMS_MESSAGE);
    }

}
