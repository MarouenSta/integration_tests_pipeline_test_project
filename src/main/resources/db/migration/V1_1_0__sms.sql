-- ajout des infos pour stocker la réception d'un statut

ALTER TABLE sms.sms_message
    ADD COLUMN status_code INTEGER;
ALTER TABLE sms.sms_message
    ADD COLUMN status_error_code VARCHAR(200);
ALTER TABLE sms.sms_message
    ADD COLUMN status_error_description VARCHAR(2000);


-- Champ pour la réception d'un message
ALTER TABLE sms.sms_message
    ADD COLUMN incoming_sms VARCHAR(1000);
ALTER TABLE sms.sms_message
    ADD COLUMN incoming_sms_date TIMESTAMP;


-- reference généré par cirrus qui permet de faire le lien entre les deux schémas/microservices.
ALTER TABLE sms.sms_message
    ADD COLUMN cirrus_ref VARCHAR(20);