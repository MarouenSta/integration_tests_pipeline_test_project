CREATE SCHEMA sms;

-- Gestion de l'historique des envois SMS pour les alerting PREVI
CREATE TABLE sms.sms_message
(
    id          SERIAL PRIMARY KEY,
    -- date d'envoi par le microservice
    send_date   TIMESTAMP,
    message     VARCHAR(1000),
    phone       VARCHAR(20),
    -- identifiant du microservice qui envoie le sms
    sender_id   VARCHAR(30),
    -- nom de l'opérateur (esendex et un futur opérateur prochainement)
    operator    VARCHAR(20),
    -- id  du sms retourné par l'opérateur
    sms_id      VARCHAR(200),
    --SMS ou VOCAL
    type        VARCHAR(10),
    --status du sms
    status      VARCHAR(20),
    --la date correspondant à la mise à jour du status
    status_date TIMESTAMP
);