package com.selerys.sms.enumeration;

import java.util.Locale;

/**
 * The code describing the current state of the message.
 */
public enum EnumCmStatus {

    UNKNOWN(-1),

    // accepted by the operator
    ACCEPTED(0),

    //rejected by CM or operator
    REJECTED(1),

    DELIVERED(2),

    //failed (message was not and will not be delivered)
    FAILED(3),

    //Read(Not for SMS, only other channels)
    READ(4);

    private int code;

    EnumCmStatus(int code) {
        this.code = code;
    }

    public static int getStatusByValue(String status) {
        for (EnumCmStatus value : EnumCmStatus.values()) {
            if (value.toString().toLowerCase(Locale.ROOT).equals(status.toLowerCase(Locale.ROOT))) {
                return value.getCode();
            }
        }
        return UNKNOWN.getCode();
    }

    public int getCode() {
        return code;
    }

    public static EnumCmStatus getStatusByCode(int code) {
        for (EnumCmStatus value : EnumCmStatus.values()) {
            if (value.getCode() == code) {
                return value;
            }
        }
        return UNKNOWN;
    }

}