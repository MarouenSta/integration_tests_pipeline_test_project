package com.selerys.sms.controller;


import com.selerys.sms.bean.ExceptionResponse;
import com.selerys.sms.exception.MissingOrInvalidFieldException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class HandlerExceptionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HandlerExceptionController.class);
    public static final String CM_PUSH_STATUS = "/cm/push/status";
    public static final String CM_PUSH_MESSAGE = "/cm/push/message";


    @ExceptionHandler(value = {Exception.class})
    public @ResponseBody ResponseEntity exception(final Exception exception, final HttpServletRequest request) {
        ExceptionResponse response = createExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), exception, request);

        //IMPORTANT : il faut toujours retourner OK à CM.com sinon ils retentent l'envoi toutes les 5 minutes pendant deux jours.
        if (CM_PUSH_STATUS.equals(request.getRequestURI()) || CM_PUSH_MESSAGE.equals(request.getRequestURI())) {
            LOGGER.info("Not able to register this cm message status ! ", exception.getMessage());
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(response,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler(value = {MissingOrInvalidFieldException.class})
    public @ResponseBody ResponseEntity missingOrInvalidFieldException(final MissingOrInvalidFieldException exception, final HttpServletRequest request) {

        ExceptionResponse response = createExceptionResponse(HttpStatus.BAD_REQUEST.toString(), exception, request);

        //IMPORTANT : il faut toujours retourner OK à CM.com sinon ils retentent l'envoi toutes les 5 minutes pendant deux jours.
        if (CM_PUSH_STATUS.equals(request.getRequestURI()) || CM_PUSH_MESSAGE.equals(request.getRequestURI())) {
            LOGGER.info("Not able to register this cm message status ! ", exception.getMessage());
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(response,HttpStatus.BAD_REQUEST);
        }
    }

    private ExceptionResponse createExceptionResponse(String statusCode, Exception exception, HttpServletRequest request) {
        ExceptionResponse response = new ExceptionResponse();
        response.setStatusCode(statusCode);
        response.setErrorMessage(exception.getMessage());
        response.callerURL(request.getRequestURI());
        response.setDate(Instant.now().toEpochMilli());
        LOGGER.error("Runtime exception in controller handler :", exception);
        return response;
    }

}