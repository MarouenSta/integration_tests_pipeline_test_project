package com.selerys.sms.controller;


import com.selerys.sms.bean.ms.TestSmsStatus;
import com.selerys.sms.exception.SmsException;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.services.SmsIncomingService;
import com.selerys.sms.services.SmsSentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller à destination de cirrus et/ou des microservices internes qui veulent envoyer un sms.
 */
@RestController
public class SmsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsController.class);

    @Autowired
    SmsSentService smsProviderService;

    @Autowired
    SmsIncomingService smsIncomingService;


    /**
     * Envoie d'un sms à un seul numéro, SANS retourner le numéro de token sms
     *
     * @return
     */
    @PostMapping(value = "/sms/send", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity sendSms(@RequestBody SmsMessage request) throws SmsException {

        smsProviderService.tryToSendSmsSendReturnToken(request);
        return new ResponseEntity(HttpStatus.OK);
    }


    /**
     * Force l'envoi d'un SMS via cm.com sans tenter un renvoi sur le backup
     *
     * @return
     */
    @PostMapping(value = "/sms/send/cm/{country}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendSmsViaCmOnly(@RequestBody SmsMessage request, @PathVariable("country") String country) throws SmsException{
        final String token = smsProviderService.tryToSendSmsOnCm(request, country);
        return new ResponseEntity(token, HttpStatus.OK);
    }


    /**
     * Envoi d'un sms et retourne le numéro de token.
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/sms/send/token", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendEsendexAndReturnToken(@RequestBody SmsMessage request) throws SmsException {

        final String token = smsProviderService.tryToSendSmsSendReturnToken(request);
        return new ResponseEntity(token, HttpStatus.OK);
    }


    /**
     * Envoi d'un sms à tous les numéros de supports qui sont paramétrés dans le fichier de configuration.
     *
     * @return
     */
    @PostMapping(value = "/sms/send/support", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity sendSupportSms(@RequestBody SmsMessage request) throws SmsException {
        smsProviderService.tryToSendSupportSMS(request);
        return new ResponseEntity(HttpStatus.OK);
    }


    /**
     * Envoi d'un sms de support sans sauvegarde en base de données
     * (pour gérer les besoins du script de monitoring postgres)
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/sms/send/support/stateless", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> sendSupportSmsWithoutDBSave(@RequestBody SmsMessage request) {
        try {
            smsProviderService.tryToSendStatelessSupportSMS(request);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("SmsController.sendSms : error when calling sendSms()", e);
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Appelé par le service monitoring pour envoyer sms et appel vocaux aux téléphones de support et d'astreintes.
     *
     * @param request
     * @param rule
     * @return
     */
    @PostMapping(value = "/sms/send/oncallduty", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendCallOnDutySmsAndVocalMessage(@RequestBody SmsMessage request, @RequestParam Boolean rule, @RequestParam Boolean alert) throws SmsException {
        smsProviderService.sendOnCallDutyMessage(rule, request, alert);
        return new ResponseEntity(HttpStatus.OK);
    }


    /**
     * Envoi seulement un sms au numéro d'astreinte
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/vocal/send/oncallduty", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendOnCallDutyVocalMessage(@RequestBody SmsMessage request) throws SmsException {
        smsProviderService.sendOnCallDutyVocalMessage(request);
        return new ResponseEntity(HttpStatus.OK);
    }


    /**
     * Envoi d'un message vocal
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/vocal/send", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendVocalMessage(@RequestBody SmsMessage request) throws SmsException {
        smsProviderService.sendVocalMessage(request);
        return new ResponseEntity(HttpStatus.OK);
    }


    /**
     * Retourne la réponse esendex
     *
     * @param smsToken
     * @return
     */
    @GetMapping(value = "/sms/status/{smsToken}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SmsMessage> getSmsStatus(@PathVariable("smsToken") String smsToken) {
        final SmsMessage smsMessage = smsIncomingService.getSmsStatus(smsToken);
        return new ResponseEntity<>(smsMessage, HttpStatus.OK);
    }


    @GetMapping(value = "/sms/incoming/{smsToken}/{phoneNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SmsMessage> getIncomingSms(@PathVariable("smsToken") String smsToken, @PathVariable("phoneNumber") String phoneNumber) {
        final SmsMessage messageHeaders = smsIncomingService.getIncomingSms(smsToken, phoneNumber);
        return new ResponseEntity<>(messageHeaders, HttpStatus.OK);
    }


    @GetMapping(value = "/sms/conf/cm/{countryCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getCmPhoneByCountry(@PathVariable("countryCode") String countryCode) {
        String phone = smsProviderService.getCmPhoneByCountry(countryCode);
        return new ResponseEntity<>(phone, HttpStatus.OK);
    }


    /**
     * Au cas ou on veuille envoyer le même sms à plusieurs numéros.
     * Le service est à implémenter.
     *
     * @param message
     * @param phone
     * @return
     */
    @PostMapping(value = "/sms/send/multi", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity sendMultipleSms(@RequestBody String message, String[] phone) throws SmsException {
        smsProviderService.sendMultiSMS(phone, message);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/sms/send/all/provider", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TestSmsStatus> sendWithAllProvider(@RequestBody SmsMessage request) {
        TestSmsStatus status = smsProviderService.sendToAllProvider(request);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

}