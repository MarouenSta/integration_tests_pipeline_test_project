package com.selerys.sms.controller;

import com.selerys.sms.bean.cm.push.SmsPush;
import com.selerys.sms.bean.cm.push.StatusPush;
import com.selerys.sms.configuration.AppConfiguration;
import com.selerys.sms.services.impl.CmPushService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * Controller qui fournit les endpoints à CM.com, pour qu'ils puissent nous envoyer des informations
 * (statut SMS, réception SMS etc)
 */
@RestController
public class CmPushController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmPushController.class);

    @Autowired
    private CmPushService pushService;

    @Autowired
    private AppConfiguration configuration;

    /**
     * Endpoint à destination de cm.com qui va nous pusher les sms qu'ils reçoivent.
     *
     * @param message
     * @param headers : headers de la requête qui nous permet de récupérer notre clef "token"
     *                (clef que nous avons paramétré sur cm.com pour qu'ils nous l'envoient.
     * @return
     */
    @PostMapping(value = "/cm/push/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> receiveCmSms(@RequestBody SmsPush message, @RequestHeader Map<String, String> headers) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("CmPushController.cmSmsStatus : réception du message : {}", message);
        }

        String authorizedToken = configuration.getAuthorisedToken();
        final String token = headers.get("token");
        if (authorizedToken.equals(token)) {
            pushService.savePushedSms(message);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            LOGGER.error("CmPushController.cmSmsStatus : error on token comparison. Received token : {}", token);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }


    /**
     * Endpoint qui permet de recevoir le status d'un sms qui vient d'être envoyé
     * <pre>
     * Note : il faut accusé réception de la réponse
     * {
     * "messages": {
     * "msg": {
     * "received": "[CREATED_S]",
     * "to": "[GSM]",
     * "reference": "[REFERENCE]",
     * "status": {
     * "code": "[STATUS]",
     * "errorCode": "[STATUSDESCRIPTION]",
     * "errorDescription": "[STANDARDERRORTEXT]"
     * },
     * "operator": "[OPERATOR]"
     * }
     * }
     * }</pre>
     */
    @PostMapping(value = "/cm/push/status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> cmSmsStatus(@RequestBody StatusPush message, @RequestHeader Map<String, String> headers) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("CmPushController.cmSmsStatus : réception du message : {}", message.toString());
        }
        String authorizedToken = configuration.getAuthorisedToken();
        final String token = headers.get("token");
        if (authorizedToken.equals(token)) {
            pushService.savePushedStatus(message);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            LOGGER.error("CmPushController.cmSmsStatus : error on token comparison. Received token : {}", token);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

}