package com.selerys.sms.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Gather all application configuration properties keys.
 *
 * @author Bastien Cramillet
 */
@Configuration
@ConfigurationProperties(prefix = "config")
public class AppConfiguration {

    private String esendexId;
    private String esendexPwd;
    private String esendexAccount;
    private String hotlineNumbers;
    private String applicationSeparator;
    private Integer maxSendForSupport;
    private Integer maxSendVocalsms;
    private String message;
    private String env;
    private String smsenvoiLogin;
    private String smsenvoiPassword;
    private String smsResetDelay;
    private String onCallDutyNumber;
    private Integer supportTempo;
    private String mainProvider;
    private String rescueProvider;

    /**
     * numéro de téléphone de l'expéditeur (donc de nous)
     */
    private String from;

    private String cmToken;

    private String cmVoiceEndpoint;

    private String cmSmsEndpoint;

    private String cmVoiceToken;

    private String cmFrom;

    private String cmVoiceFrom;

    private String authorisedToken;

    private String cmFromDefault;

    private String cirrusUrl;

    private String esendexConversationUri;

    private String esendexConversationEndPoint;

    private String esendexMessageHeadersUri;

    private String esendexMessageHeadersEndPoint;

    public String getEsendexConversationUri() {
        return esendexConversationUri;
    }

    public void setEsendexConversationUri(String esendexConversationUri) {
        this.esendexConversationUri = esendexConversationUri;
    }

    public String getEsendexConversationEndPoint() {
        return esendexConversationEndPoint;
    }

    public void setEsendexConversationEndPoint(String esendexConversationEndPoint) {
        this.esendexConversationEndPoint = esendexConversationEndPoint;
    }

    public String getEsendexMessageHeadersUri() {
        return esendexMessageHeadersUri;
    }

    public void setEsendexMessageHeadersUri(String esendexMessageHeadersUri) {
        this.esendexMessageHeadersUri = esendexMessageHeadersUri;
    }

    public String getEsendexMessageHeadersEndPoint() {
        return esendexMessageHeadersEndPoint;
    }

    public void setEsendexMessageHeadersEndPoint(String esendexMessageHeadersEndPoint) {
        this.esendexMessageHeadersEndPoint = esendexMessageHeadersEndPoint;
    }

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCirrusUrl() {
        return cirrusUrl;
    }

    public void setCirrusUrl(String cirrusUrl) {
        this.cirrusUrl = cirrusUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMaxSendVocalsms() {
        return maxSendVocalsms;
    }

    public String getSmsResetDelay() {
        return smsResetDelay;
    }

    public void setSmsResetDelay(String smsResetDelay) {
        this.smsResetDelay = smsResetDelay;
    }

    public void setMaxSendVocalsms(Integer maxSendVocalsms) {
        this.maxSendVocalsms = maxSendVocalsms;
    }

    public Integer getMaxSendForSupport() {
        return maxSendForSupport;
    }

    public void setMaxSendForSupport(Integer maxSendForSupport) {
        this.maxSendForSupport = maxSendForSupport;
    }

    public String getEsendexId() {
        return esendexId;
    }

    public void setEsendexId(String esendexId) {
        this.esendexId = esendexId;
    }

    public String getEsendexPwd() {
        return esendexPwd;
    }

    public void setEsendexPwd(String esendexPwd) {
        this.esendexPwd = esendexPwd;
    }

    public String getEsendexAccount() {
        return esendexAccount;
    }

    public void setEsendexAccount(String esendexAccount) {
        this.esendexAccount = esendexAccount;
    }

    public String getHotlineNumbers() {
        return hotlineNumbers;
    }

    public void setHotlineNumbers(String hotlineNumbers) {
        this.hotlineNumbers = hotlineNumbers;
    }

    public String getApplicationSeparator() {
        return applicationSeparator;
    }

    public void setApplicationSeparator(String applicationSeparator) {
        this.applicationSeparator = applicationSeparator;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSmsenvoiLogin() {
        return smsenvoiLogin;
    }

    public void setSmsenvoiLogin(String smsenvoiLogin) {
        this.smsenvoiLogin = smsenvoiLogin;
    }

    public String getSmsenvoiPassword() {
        return smsenvoiPassword;
    }

    public void setSmsenvoiPassword(String smsenvoiPassword) {
        this.smsenvoiPassword = smsenvoiPassword;
    }

    public String getOnCallDutyNumber() {
        return onCallDutyNumber;
    }

    public void setOnCallDutyNumber(String onCallDutyNumber) {
        this.onCallDutyNumber = onCallDutyNumber;
    }


    public String getCmToken() {
        return cmToken;
    }

    public void setCmToken(String cmToken) {
        this.cmToken = cmToken;
    }

    public String getCmVoiceEndpoint() {
        return cmVoiceEndpoint;
    }

    public void setCmVoiceEndpoint(String cmVoiceEndpoint) {
        this.cmVoiceEndpoint = cmVoiceEndpoint;
    }

    public String getCmSmsEndpoint() {
        return cmSmsEndpoint;
    }

    public void setCmSmsEndpoint(String cmSmsEndpoint) {
        this.cmSmsEndpoint = cmSmsEndpoint;
    }


    public String getCmVoiceToken() {
        return cmVoiceToken;
    }

    public void setCmVoiceToken(String cmVoiceToken) {
        this.cmVoiceToken = cmVoiceToken;
    }


    public String getCmFrom() {
        return getCmFrom("FR");
    }

    /**
     * Retourne le numéro CM à utiliser selon le pays cible.
     *
     * @param countryCode
     * @return
     */
    public String getCmFrom(String countryCode) {
        final String[] froms = this.cmFrom.split(";");
        for (int i = 0; i < froms.length; i++) {
            final String from = froms[i];
            final String[] keyValuefrom = from.split("-");
            if (keyValuefrom[1].toUpperCase().equals(countryCode.toUpperCase())) {
                return keyValuefrom[0];
            }
        }
        return this.cmFromDefault;
    }

    public void setCmFrom(String cmFrom) {
        this.cmFrom = cmFrom;
    }

    public Integer getSupportTempo() {
        return supportTempo;
    }

    public void setSupportTempo(Integer supportTempo) {
        this.supportTempo = supportTempo;
    }

    public String getAuthorisedToken() {
        return authorisedToken;
    }

    public void setAuthorisedToken(String authorisedToken) {
        this.authorisedToken = authorisedToken;
    }

    public String getMainProvider() {
        return mainProvider;
    }

    public void setMainProvider(String mainProvider) {
        this.mainProvider = mainProvider;
    }


    public String getCmVoiceFrom() {
        return cmVoiceFrom;
    }

    public void setCmVoiceFrom(String cmVoiceFrom) {
        this.cmVoiceFrom = cmVoiceFrom;
    }

    public String getRescueProvider() {
        return rescueProvider;
    }

    public void setRescueProvider(String rescueProvider) {
        this.rescueProvider = rescueProvider;
    }


    public String getCmFromDefault() {
        return cmFromDefault;
    }

    public void setCmFromDefault(String cmFromDefault) {
        this.cmFromDefault = cmFromDefault;
    }
}