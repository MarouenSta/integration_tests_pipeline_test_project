package com.selerys.sms.configuration;

import com.selerys.sms.enumeration.EnumOperator;
import com.selerys.sms.repository.SmsMessageRepository;
import com.selerys.sms.services.MarshallingService;
import com.selerys.sms.services.SmsService;
import com.selerys.sms.services.impl.CmService;
import com.selerys.sms.services.impl.EsendexService;
import com.selerys.sms.services.impl.SMSEnvoiService;
import com.selerys.sms.utils.PhoneNumberValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration qui permet d'instancier le smsService en fonction d'une valeur dans le fichier application.properties.
 * Ce smsService sera donc l'expéditeur principal pour tout le MS.
 */
@Configuration
public class SmsConfiguration {

    @Autowired
    AppConfiguration configuration;

    @Autowired
    SmsMessageRepository smsMessageRepository;

    @Autowired
    PhoneNumberValidationUtil phoneNumberValidationUtil;

    @Autowired
    MarshallingService marshallingService;

    @Bean("mainProvider")
    public SmsService getSmsService() {
        return this.serviceFactory(configuration.getMainProvider());
    }

    @Bean("rescueProvider")
    public SmsService getRescueSmsService() {
        return this.serviceFactory(configuration.getRescueProvider());
    }

    /**
     * Choix de l'implémentation d'un service en fonction de la configuration
     *
     * @return
     */
    public SmsService serviceFactory(String provider) {
        if (configuration != null) {
            if (EnumOperator.CM.toString().equals(provider)) {
                return new CmService(configuration, smsMessageRepository, phoneNumberValidationUtil);
            } else if (EnumOperator.ESENDEX.toString().equals(provider)) {
                return new EsendexService(configuration, smsMessageRepository, marshallingService);
            } else {
                return new SMSEnvoiService(configuration, smsMessageRepository);
            }
        } else {
            return new CmService(configuration, smsMessageRepository, phoneNumberValidationUtil);
        }
    }

}