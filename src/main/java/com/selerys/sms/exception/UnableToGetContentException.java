package com.selerys.sms.exception;

public class UnableToGetContentException extends Exception {

    public UnableToGetContentException(String message) {
        super(message);
    }
}
