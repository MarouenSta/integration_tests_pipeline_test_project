package com.selerys.sms.exception;

public class SmsException extends Exception {

    public SmsException() {
    }

    public SmsException(String message) {
        super(message);
    }

    public SmsException(String message, Throwable cause) {
        super(message, cause);
    }

    public SmsException(Exception e) {
        super(e);
    }
}
