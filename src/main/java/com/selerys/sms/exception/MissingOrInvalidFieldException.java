package com.selerys.sms.exception;

public class MissingOrInvalidFieldException extends RuntimeException{

    public MissingOrInvalidFieldException(String ss){
        super("there was a missing or invalid field in the request");
    }

    public MissingOrInvalidFieldException(String missingFieldName, Class<?> missingFieldType){
        super(missingFieldName+" of type "+missingFieldType+" is invalid or missing !");
    }

}
