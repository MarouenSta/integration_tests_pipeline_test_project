package com.selerys.sms.exception;

public class UnableToGetContent extends Exception {

    public UnableToGetContent(String message) {
        super(message);
    }
}
