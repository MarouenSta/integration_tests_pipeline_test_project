package com.selerys.sms.exception;

public class BadResponseFromEsendex extends Exception {
    public BadResponseFromEsendex(String message) {
        super(message);
    }

    public BadResponseFromEsendex(String message, Throwable cause) {
        super(message, cause);
    }
}
