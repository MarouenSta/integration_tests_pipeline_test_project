package com.selerys.sms.exception;

public class SmsOperationException extends RuntimeException{
    public SmsOperationException(String message) {
        super(message);
    }
}
