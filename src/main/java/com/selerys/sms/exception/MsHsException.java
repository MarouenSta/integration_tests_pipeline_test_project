package com.selerys.sms.exception;

/**
 * Exception levée lorsque les MS ne répondent pas.
 */
public class MsHsException extends RuntimeException {

	public MsHsException(String message) {
		super(message);
	}

}
