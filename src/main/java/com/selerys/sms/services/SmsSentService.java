package com.selerys.sms.services;

import com.selerys.sms.bean.ms.TestSmsStatus;
import com.selerys.sms.configuration.AppConfiguration;
import com.selerys.sms.exception.MissingOrInvalidFieldException;
import com.selerys.sms.exception.SmsException;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.services.impl.CmService;
import com.selerys.sms.utils.PhoneNumberValidationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionException;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class SmsSentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsSentService.class);

    @Autowired
    private PhoneNumberValidationUtil phoneNumberValidationUtil;

    @Autowired
    @Qualifier("mainProvider")
    private SmsService mainSmsService;

    @Autowired
    @Qualifier("rescueProvider")
    private SmsService rescueSmsService;

    @Autowired
    private CmService cmService;

    @Autowired
    private AppConfiguration configuration;

    /**
     * @param smsMessage
     * @return
     * @throws SmsException
     */
    public String tryToSendSmsSendReturnToken(SmsMessage smsMessage) throws SmsException {
        //Obligé de vérifier la nullité aussi haut pour ne pas envoyer de smsEnvoi en cas de phone null.

        if (phoneNumberValidationUtil.isPhoneOk(smsMessage.getPhone())) {
            try {
                return mainSmsService.sendSmsAndGetToken(smsMessage);
            } catch (TransactionException transactionException) {
                LOGGER.error("TRANSACTION EXCEPTION : Unable to send a sms log in database. Send a stateless sms");
                this.tryToSendStatelessSupportSMS(smsMessage);
                return "NoToken";
            } catch (Exception e) {
                LOGGER.error("SmsProviderService.tryToSendSmsSendReturnToken : error when calling smsService.sendSms()", e);
                LOGGER.error(e.getMessage());

                try {
                    LOGGER.info("Try to send sms via rescue prestataire");
                    return rescueSmsService.sendSmsAndGetToken(smsMessage);
                } catch (SmsException ex) {
                    LOGGER.error(ex.getMessage());
                    throw new SmsException("SmsProviderService.tryToSendSmsSendReturnToken : Error when sending sms with main and rescue provider ! " + smsMessage.toString());
                }
            }
        } else {
            throw new MissingOrInvalidFieldException("phone",String.class);
        }
    }

    /**
     * Allow to send a message with both provider (main and rescue)
     * This is used to be sure that both number are allowed on target phone number
     *
     * @param smsMessage
     * @return
     */
    public TestSmsStatus sendToAllProvider(final SmsMessage smsMessage) {
        TestSmsStatus testSmsStatus = new TestSmsStatus();
        testSmsStatus.setSuccess(true);
        SmsMessage smsProvider1 = new SmsMessage(smsMessage);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;
        String now = LocalDateTime.now().atZone(ZoneId.of("Europe/Paris")).format(dateTimeFormatter);
        smsProvider1.setMessage(now + " Message 1/2 : " + smsMessage.getMessage());
        try {
            mainSmsService.sendSmsAndGetToken(smsProvider1);
        } catch (SmsException e) {
            testSmsStatus.setSuccess(false);
            testSmsStatus.getDetail().put(mainSmsService.getClass().getName(), Boolean.FALSE);
            LOGGER.error("SmsProviderService.tryToSendSmsSendReturnToken : Error when sending sms with main provider" + smsMessage.toString());
        }
        SmsMessage smsProvider2 = new SmsMessage(smsMessage);
        smsProvider2.setMessage(now + " Message 2/2 : " + smsMessage.getMessage());
        try {
            rescueSmsService.sendSmsAndGetToken(smsProvider2);
        } catch (SmsException e) {
            testSmsStatus.setSuccess(false);
            testSmsStatus.getDetail().put(rescueSmsService.getClass().getName(), Boolean.FALSE);
            LOGGER.error("SmsProviderService.tryToSendSmsSendReturnToken : Error when sending sms with rescue provider" + smsMessage.toString());
        }
        return testSmsStatus;
    }

    /**
     * Envoi d'un message via le prestataire cm.com (envoi forcé).
     * Normalement, utile uniquement pour les soblis.
     *
     * @param smsMessage
     * @throws SmsException
     */
    public String tryToSendSmsOnCm(SmsMessage smsMessage, String country) throws SmsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("CmService.tryToSendSmsOnCm : send message {}", smsMessage.toString());
        }
        return cmService.sendSmsForCountry(smsMessage, country);
    }


    /**
     * envoi un message au support en priorité via esendex. Si esendex HS, tente un envoi par SMSENVOI.
     *
     * @param request
     */
    public void tryToSendSupportSMS(SmsMessage request) throws SmsException {


        try {
            mainSmsService.sendSupportSMS(request);
        } catch (TransactionException transactionException) {
            LOGGER.error("TRANSACTION EXCEPTION : Unable to send a sms log in database. Send a stateless sms");
            this.tryToSendStatelessSupportSMS(request);
        } catch (Exception e) {
            LOGGER.error("SmsController.tryToSendSupportSMS : error when calling smsService.tryToSendSupportSMS()", e);
            LOGGER.error(e.getMessage());
            try {
                rescueSmsService.sendSupportSMS(request);
            } catch (SmsException ex) {
                LOGGER.error(ex.getMessage());
                LOGGER.debug("SmsController.tryToSendSupportSMS : Erreur d'envoi du sms par le prestataire principal ET par secondaire pour le message " + request.toString());
                throw ex;
            }
        }
    }


    /**
     * Envoi d'un sms sans aucune utilisation de la base de données (au cas ou celle-ci ne fonctionne plus).
     * Induit une non vérification des quotas, et le non enregistrement des messages (donc une non gestion des retour de status).
     *
     * @param request
     * @throws SmsException
     */
    public void tryToSendStatelessSupportSMS(SmsMessage request) throws SmsException {
        try {
            mainSmsService.sendStatelessSupportSms(request);
        } catch (Exception e) {
            LOGGER.error("SmsController.tryToSendSupportSMS : error when calling smsService.tryToSendStatelessSupportSMS()", e);
            LOGGER.error(e.getMessage());
            try {
                rescueSmsService.sendStatelessSupportSms(request);
            } catch (SmsException ex) {
                LOGGER.error(ex.getMessage());
                throw new SmsException("SmsController.tryToSendSupportSMS : Erreur d'envoi du sms par esendex ET par smsenvoi pour le message " + request.toString());
            }
        }
    }


    /**
     * Envoi d'un appel vocal via esendex uniquement. Non disponible sur SMSenvoi.
     * (dans tous les cas, un sms est aussi envoyé par l'application qui tente un envoi vocal).
     *
     * @param request
     */
    public void sendVocalMessage(SmsMessage request) throws SmsException {
        try {
            mainSmsService.sendVocalMessage(request);
        } catch (Exception e) {
            LOGGER.error("SmsController.sendOnCallDutyCall : error when calling smsService.sendOnCallDutyCall()", e);
            LOGGER.error(e.getMessage());
        }
    }


    /**
     * Envoi d'un message vocal au téléphone d'astreinte
     *
     * @param request
     * @throws SmsException
     */
    public void sendOnCallDutyVocalMessage(SmsMessage request) throws SmsException {
        if (configuration.getOnCallDutyNumber() != null && !configuration.getOnCallDutyNumber().isEmpty()) {
            request.setPhone(configuration.getOnCallDutyNumber());
            mainSmsService.sendVocalMessage(request);
        }
    }


    /**
     * Fait le nécessaire pour envoyer un message au support et aux téléphones d'astreintes.
     *
     * @param onCallDuty
     * @param smsMessage
     * @param alerte
     */
    public void sendOnCallDutyMessage(boolean onCallDuty, SmsMessage smsMessage, boolean alerte) throws SmsException {
        if (configuration.getOnCallDutyNumber() != null && !configuration.getOnCallDutyNumber().isEmpty()) {
            SmsMessage smsVoixToOnCallDuty = new SmsMessage();
            smsVoixToOnCallDuty.setMessage(smsMessage.getMessage());
            smsVoixToOnCallDuty.setPhone(configuration.getOnCallDutyNumber());
            smsVoixToOnCallDuty.setSenderId(smsMessage.getSenderId());
            smsVoixToOnCallDuty.setSendDate(smsMessage.getSendDate());
            if (onCallDuty) {
                if (alerte) {
                    this.sendVocalMessage(smsVoixToOnCallDuty);
                }
                //On décale de 0 seconde l'envoi du sms de l'appel afin d'éviter un conflit chez esendex
                if (!configuration.getHotlineNumbers().contains(configuration.getOnCallDutyNumber())) {
                    SmsMessage smsToOnCallDuty = new SmsMessage();
                    smsToOnCallDuty.setMessage(smsMessage.getMessage());
                    smsToOnCallDuty.setPhone(configuration.getOnCallDutyNumber());
                    smsToOnCallDuty.setSenderId(smsMessage.getSenderId());
                    smsToOnCallDuty.setSendDate(smsMessage.getSendDate());
                    Runnable task = () -> {
                        try {
                            this.tryToSendSmsSendReturnToken(smsToOnCallDuty);
                        } catch (SmsException e) {
                            LOGGER.error("SmsProviderService.sendOnCallDutyMessage : erreur sur tryToSendSmsSendReturnToken", e);
                        }
                    };
                    this.delaySmsTaskExecutor(task, 30);
                }
            }
        }
        Runnable task = () -> {
            try {
                this.tryToSendSupportSMS(smsMessage);
            } catch (SmsException e) {
                LOGGER.error("SmsProviderService.sendOnCallDutyMessage : erreur sur tryToSendSupportSMS", e);
            }
        };
        this.delaySmsTaskExecutor(task, 30);
    }


    private void delaySmsTaskExecutor(Runnable task, Integer delay) {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.schedule(task, delay, TimeUnit.SECONDS);
        // and finally, when your program wants to exit
        executor.shutdown();
    }



    public void sendMultiSMS(String[] phone, String message) throws SmsException {
        try {
            mainSmsService.sendMultiSMS(phone, message);
        } catch (Exception e) {
            LOGGER.error("SmsProviderService.sendMultiSMS : error when calling smsService.sendMultiSMS()", e);
            LOGGER.error(e.getMessage());
            try {
                rescueSmsService.sendMultiSMS(phone, message);
            } catch (SmsException ex) {
                LOGGER.error(ex.getMessage());
                throw new SmsException("SmsProviderService.sendMultiSMS : Erreur d'envoi sendMultiSMS par esendex ET par smsenvoi pour le message " + message);
            }
        }
    }

    /**
     * pemet de déterminer quel est le code pays à utiliser.
     *
     * @param countryCode
     * @return
     */
    public String getCmPhoneByCountry(String countryCode) {
        return configuration.getCmFrom(countryCode);
    }

}