package com.selerys.sms.services;

import com.selerys.sms.exception.BadResponseFromEsendex;
import com.selerys.sms.exception.SmsException;
import com.selerys.sms.exception.SmsOperationException;
import com.selerys.sms.exception.UnableToGetContent;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.repository.SmsMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.ParseException;

@Service
public class SmsIncomingService {

    private static final String ESENDEX = "ESENDEX";
    private static final String SMS_ENVOI = "SMS_ENVOI";
    private static final String CM = "CM";

    @Autowired
    private SmsMessageRepository messageRepository;

    @Autowired
    @Qualifier("esendexService")
    private SmsService esendexService;

    @Autowired
    @Qualifier("SMSEnvoiService")
    private SmsService smsEnvoiService;


    /**
     * Action différente en fonction du prestataire qui a servi à envoyé le sms.
     *
     * @return
     * @throws BadResponseFromEsendex
     * @throws IOException
     * @throws UnableToGetContent
     */
    public SmsMessage getIncomingSms(String smsToken, String phoneNumber) {
        SmsMessage responses = new SmsMessage();
        try {
            //on est obligé de vérifier qui a été le prestataire sur l'expédition du sms
            SmsMessage sms = messageRepository.findSmsByToken(smsToken);
            if (sms != null) {
                switch (sms.getOperator()) {
                    case ESENDEX:
                        responses = esendexService.getSmsResponse(smsToken, phoneNumber);
                        break;
                    case SMS_ENVOI:
                        responses = smsEnvoiService.getSmsResponse(smsToken, phoneNumber);
                        break;
                    case CM:
                        //Pour CM, la mise à jour est en mode push. Le push de son côté fait sa vie pour mettre à jour en base l'objet SmsMessage.
                        //Il suffit donc de retourner cet objet.
                        responses = sms;
                        break;
                    default:
                        responses = sms;
                }
            }
        } catch (BadResponseFromEsendex e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        } catch (UnableToGetContent e) {
            throw new RuntimeException(e);
        }
        return responses;
    }

    /**
     * Permet de récupérer le statut d'un sms déjà envoyé.
     * Va vérifier par quel fournisseur nous sommes passés lors de l'envoi pour taper sur le bon provider.
     *
     * @param smsToken
     * @return
     */
    public SmsMessage getSmsStatus(String smsToken) {
        try {
            SmsMessage sms = messageRepository.findSmsByToken(smsToken);
            if (sms != null) {
                SmsMessage status = null;
                switch (sms.getOperator()) {
                    case ESENDEX:
                        status = esendexService.getSmsStatus(smsToken);
                        break;
                    case SMS_ENVOI:
                        status = smsEnvoiService.getSmsStatus(smsToken);
                        break;
                    case CM:
                        //dans le cas de CM, la mise à jour du statut ne doit pas être faite "à la demande", elle est faite en mode push automatiquement.
                        //il suffit donc de retourner le sms qui nous a servi à vérifier l'opérateur.
                        status = sms;
                        break;
                    default:
                        status = sms;
                }
                return status;
            }
        } catch (SmsException | IOException e) {
            throw new SmsOperationException("Unable to get Status for token : " + smsToken);
        }
        throw new SmsOperationException("sms not find by token " + smsToken);
    }

}