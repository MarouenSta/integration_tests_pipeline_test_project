package com.selerys.sms.services.listener;

import com.selerys.sms.exception.SmsException;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.services.SmsSentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class StartingOnCallDutyListener {

    private static final Logger LOGGER = LogManager.getLogger(StartingOnCallDutyListener.class);

    @Autowired
    private SmsSentService smsProviderService;

    @EventListener(ApplicationReadyEvent.class)
    public void startOnCallDuty() {
        SmsMessage smsMessage = new SmsMessage();
        smsMessage.setMessage("Salut ! Ton numéro sert d'astreinte cette semaine ! Je te souhaite bonne chance !");
        smsMessage.setSendDate(LocalDateTime.now());
        try {
            smsProviderService.sendOnCallDutyVocalMessage(smsMessage);
        } catch (SmsException e) {
            LOGGER.error("Unable to start on call duty ! ", e);
        }
    }
}
