package com.selerys.sms.services;

import com.selerys.sms.bean.esendex.MessageBody;
import com.selerys.sms.bean.esendex.MessageHeader;
import com.selerys.sms.bean.esendex.MessageHeaders;
import com.selerys.sms.exception.UnableToGetContent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


@Service
public class MarshallingService {

    private static final Logger LOGGER = LogManager.getLogger(MarshallingService.class);
    public static final String CHARSET_UTF8 = "UTF-8";

    /**
     * Méthode qui permet de parser un contenu de type {@link MessageHeader} à partir d'un contenu sous forme de string.
     *
     * @param content
     * @return
     * @throws UnableToGetContent
     */
    public MessageHeader unmarshalMessageHeader(String content) throws UnableToGetContent {
        try {
            JAXBContext jc = JAXBContext.newInstance(MessageHeader.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            InputStream inputStream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
            return (MessageHeader) unmarshaller.unmarshal(inputStream);
        } catch (JAXBException e) {
            LOGGER.error("Error when unmarshalling MessageHeader from esendex content is : {}", content, e);
            throw new UnableToGetContent("unable to get content from message header from esendex !");
        }
    }

    public MessageBody unmarshalMessageBody(String content) throws UnableToGetContent {
        try {
            JAXBContext jc = JAXBContext.newInstance(MessageBody.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            InputStream inputStream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
            return (MessageBody) unmarshaller.unmarshal(inputStream);
        } catch (JAXBException e) {
            LOGGER.error("Error when unmarshalling MessageBody from esendex is : {}", content, e);
            throw new UnableToGetContent("unable to get content from message header from esendex !");
        }
    }


    /**
     * Parse un objet de type {@link MessageHeaders} (donc qui contient plusieurs MessageHeader) à partir d'une string.
     *
     * @param content
     * @return
     * @throws UnableToGetContent
     */
    public MessageHeaders unmarshalMessageHeaders(String content) throws UnableToGetContent {
        try {
            JAXBContext jc = JAXBContext.newInstance(MessageHeaders.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            InputStream inputStream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
            return (MessageHeaders) unmarshaller.unmarshal(inputStream);
        } catch (JAXBException e) {
            LOGGER.error("Error when unmarshalling the response from esendex content is : {}", content, e);
            throw new UnableToGetContent("unable to get content from esendex header from esendex !");
        }
    }

}



