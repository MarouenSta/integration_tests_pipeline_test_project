
package com.selerys.sms.services;

import com.selerys.sms.exception.SmsException;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ApplicationShutdown implements ApplicationListener<ContextClosedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationShutdown.class);

    @Autowired
    private SmsSentService smsService;

/**
     * Gestion de la fermeture de l'application
     *
     * @param event
     */


    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        SmsMessage message = new SmsMessage();
        message.setMessage("sms_ms is shutting down.");
        message.setSenderId("SMS_MS");
        message.setSendDate(LocalDateTime.now());
        try {
            smsService.tryToSendSupportSMS(message);
        } catch (SmsException e) {
            LOGGER.error("Error when trying to send sms on close SMS_MS : {}", e.getMessage());
        }
    }

}
