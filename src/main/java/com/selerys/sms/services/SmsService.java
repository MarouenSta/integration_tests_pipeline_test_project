package com.selerys.sms.services;

import com.selerys.sms.exception.*;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import esendex.sdk.java.EsendexException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.ParseException;

public interface SmsService {

    /**
     * Envoi un sms, l'enregistre en base et retourne son ID
     *
     * @param smsMessage
     * @return
     * @throws EsendexException
     */
    @Transactional
    String sendSmsAndGetToken(SmsMessage smsMessage) throws SmsException;


    /**
     * Envoi de sms sans enregistrement en base de données, sans vérification de quota etc.
     * Faite pour le script de monitoring de failover postgres. Ne pas utiliser en temps normal.
     *
     * @param smsMessage
     * @throws SmsException
     */
    void sendStatelessSupportSms(SmsMessage smsMessage) throws SmsException;

    /**
     * Envoi d'un sms aux numéros de support (et l'enregistre en base)
     *
     * @param smsMessage
     * @throws SmsException
     */
    @Transactional
    void sendSupportSMS(SmsMessage smsMessage) throws SmsException;

    /**
     * Interroge le prestataire SMS et retourne le statut du sms dont l'id est passé en argument
     *
     * @param token
     * @return
     * @throws IOException
     * @throws BadResponseFromEsendex
     * @throws UnableToGetContentException
     */
    SmsMessage getSmsStatus(String token) throws SmsException, IOException;

    /**
     * Récupère un sms
     *
     * @param token
     * @return
     */
    SmsMessage getSmsResponse(String token, String phoneNumber) throws BadResponseFromEsendex, IOException, UnableToGetContent, ParseException;

    /**
     * Envoi d'un message vocal
     *
     * @param message
     * @return
     * @throws SmsException
     */
    @Transactional
    void sendVocalMessage(SmsMessage message) throws SmsException;


    /**
     * Envoi le même sms aux numéros de téléphone passés en argument.
     *
     * @param phone
     * @param message
     */
    @Transactional
    void sendMultiSMS(String[] phone, String message) throws SmsException;

}
