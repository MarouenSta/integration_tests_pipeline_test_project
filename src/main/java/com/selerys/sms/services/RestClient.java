package com.selerys.sms.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.selerys.sms.bean.cm.HttpResponseBody;
import com.selerys.sms.exception.SmsException;
import org.springframework.http.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

/**
 * J'ai envie de faire un truc plus clean que ce qui a été fait pour esendex, et utiliser les possibilités de spring
 * pour éviter de faire appel à d'autres librairies lorsqu'on veut faire des requêtes http.
 */
public class RestClient<T> {


    private String url;
    private RestTemplate rest;
    private HttpHeaders headers;
    private HttpStatus status;

    public RestClient(String url, HttpHeaders headers) {
        this.rest = new RestTemplate();
        this.url = url;
        this.headers = headers;
    }

    public String get() {
        HttpEntity<String> requestEntity = new HttpEntity<>("", this.headers);
        ResponseEntity<String> responseEntity = rest.exchange(this.url, HttpMethod.GET, requestEntity, String.class);
        this.setStatus(responseEntity.getStatusCode());
        return responseEntity.getBody();
    }


    /**
     * Post générique
     *
     * @param request
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<HttpResponseBody> post(T request) throws SmsException {
        HttpEntity<T> requestEntity = new HttpEntity<>(request, this.headers);
        try {
            return rest.exchange(this.url, HttpMethod.POST, requestEntity, HttpResponseBody.class);
        } catch (HttpStatusCodeException e) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                HttpResponseBody responseBody = mapper.readValue(e.getResponseBodyAsString(), HttpResponseBody.class);
                return new ResponseEntity<>(responseBody, e.getResponseHeaders(), e.getStatusCode());
            } catch (JsonProcessingException ex) {
                throw new SmsException("RestClient.post : error while trying to parse HttpStatusCodeException.getResponseBodyAsString");
            }
        }
    }


    public ResponseEntity<String> voicePost(T request) {
        HttpEntity<T> requestEntity = new HttpEntity<>(request, this.headers);
        //TODO : vérifier si tous les types de messages CM.com retourne un HttpResponseBody sinon va falloir "générifier" ça
        return rest.exchange(this.url, HttpMethod.POST, requestEntity, String.class);
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

}