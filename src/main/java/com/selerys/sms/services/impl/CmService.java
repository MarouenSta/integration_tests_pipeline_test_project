package com.selerys.sms.services.impl;

import com.selerys.sms.bean.cm.*;
import com.selerys.sms.configuration.AppConfiguration;
import com.selerys.sms.enumeration.EnumCmStatus;
import com.selerys.sms.enumeration.EnumOperator;
import com.selerys.sms.exception.BadResponseFromEsendex;
import com.selerys.sms.exception.MissingOrInvalidFieldException;
import com.selerys.sms.exception.SmsException;
import com.selerys.sms.exception.UnableToGetContent;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.repository.SmsMessageRepository;
import com.selerys.sms.services.RestClient;
import com.selerys.sms.services.SmsService;
import com.selerys.sms.utils.PhoneNumberValidationUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Service("cmService")
public class CmService implements SmsService {

    private static final Logger LOGGER = LogManager.getLogger();

    public static final String X_CM_PRODUCTTOKEN = "X-CM-PRODUCTTOKEN";

    //Les téléphones "to" doivent être de la forme 0033XXXXXXXXX
    public static final String PHONE_PREFIX = "00";

    private SmsMessageRepository smsMessageRepository;

    private AppConfiguration configuration;
    static Integer count = 0;

    private PhoneNumberValidationUtil phoneNumberValidationUtil;
    @Autowired
    public CmService(AppConfiguration configuration, SmsMessageRepository smsMessageRepository, PhoneNumberValidationUtil phoneNumberValidationUtil) {
        LOGGER.info("Appel du constructeur CmService");
        this.configuration = configuration;
        this.smsMessageRepository = smsMessageRepository;
        this.phoneNumberValidationUtil = phoneNumberValidationUtil;
    }

    public CmService() {
    }


    /**
     * Règle CM : pour recevoir une réponse à un sms, il faut mettre comme numéro expéditeur le 38082.
     *
     * @param smsMessage
     * @return
     * @throws SmsException
     */
    @Override
    public String sendSmsAndGetToken(SmsMessage smsMessage) throws SmsException {
        return sendSmsForCountry(smsMessage, "FR");
    }


    public String sendSmsForCountry(SmsMessage smsMessage, String countryCode) throws SmsException {

        String result = "";

        //vérification de contenu de message
        if(smsMessage.getMessage() == null || smsMessage.getMessage().trim().isEmpty()){
            LOGGER.error("CmService.sendSmsForCountry : le contenu de message envoyé est vide");
            throw new MissingOrInvalidFieldException("message",String.class);
        }

        //Les services appelants valorisent sendDate, MAIS ils ne donnent pas la valeur en UTC.
        //Pour éviter de modifier tous nos MS, je repasse dessus (le sendDate dans les services appelant est valorisé avec LocalDateTime.now())
        smsMessage.setSendDate(LocalDateTime.now(ZoneOffset.UTC));
        smsMessage.setType("SMS");
        smsMessage.setOperator(EnumOperator.CM.toString());

        //On crée d'abord le message en base de données pour pouvoir récupérer son ID et ainsi le passer à cm.
        //on en a en effet besoin pour mettre à jour le sms par la suite, lorsqu'il vont nous envoyer son status.
        final SmsMessage newlyCreatedMessage = smsMessageRepository.create(smsMessage);
        smsMessage.setId(newlyCreatedMessage.getId());

        //vérifier le numéro de tel
        if(!phoneNumberValidationUtil.isPhoneOk(smsMessage.getPhone())){
            throw new MissingOrInvalidFieldException("phone",String.class);
        }

        //Au final, pas besoin de faire une vérif pour appeler convertMessageIntoSmsRequest ou convertMessageIntoSmsRequestWithFromI18N,
        final CmSmsRequest request = this.convertMessageIntoSmsRequestWithFromI18N(smsMessage, countryCode);

        HttpHeaders headers = this.generateHeaders(configuration.getCmToken());
        RestClient<CmSmsRequest> client = new RestClient<>(configuration.getCmSmsEndpoint(), headers);
        final ResponseEntity<HttpResponseBody> response = client.post(request);
        //Changement de logique : on mets à jour la donnée quoi qu'il arrive et s'il y a eu une erreur, on lève une exception à la fin pour tenter un envoi avec le backup.
        if (response != null && response.getBody() != null) {
            LOGGER.debug("CmService.sendSmsAndGetToken : valeur de response sur envoi du message {} : {}", smsMessage.getId(), response);
            final ResponseMessageDetail[] messages = response.getBody().getMessages();
            if (messages != null && messages.length == 1) {
                //Dans le cas de cm, ils ne nous retournent pas d'identifiants comme le fait esendex. On fait donc le lien avec l'id postgres.
                smsMessageRepository.updateSmsToken(smsMessage, newlyCreatedMessage.getId().toString());

                // L'api cm.com n'est pas très propre sur la gestion des status. Il y a deux manières différentes de gérer un statut :
                // par retour direct sur l'envoi, et par réception d'un status/push
                // Dans le cas présent, elle retourne un objet comme ça :
                // "messages": [
                //    {
                //      "to": "0044791112345a",
                //      "status": "Rejected",
                //      "reference": "your_reference_A",
                //      "parts": 0,
                //      "messageDetails": "Gsm '0044791112345a' is not a number.",
                //      "messageErrorCode": 303 //See table "JSON POST Error codes" below
                //    },
                // la ligne "status": "Rejected" est celle qui nous intéresse
                // dans un status push, il y a un code pour les statuts. Ce qui engendre deux gestions différentes

                smsMessage.setStatus(messages[0].getStatus());
                int statusCode = EnumCmStatus.getStatusByValue(messages[0].getStatus());
                smsMessage.setStatusCode(statusCode);
                smsMessage.setStatusErrorCode(messages[0].getMessageErrorCode());
                smsMessage.setStatusErrorDescription(messages[0].getMessageDetails());
                smsMessageRepository.updateStatus(smsMessage);

                result = newlyCreatedMessage.getId().toString();
            }

            if (!response.getStatusCode().is2xxSuccessful()) {
                throw new SmsException("CmService.sendSmsAndGetToken : Error while trying to send message to " + smsMessage.getPhone() + ". " + response.getStatusCodeValue());
            }
        } else {
            LOGGER.info("CmService.sendSmsAndGetToken : response is null when trying to send message {}", smsMessage);
        }
        return result;
    }


    private HttpHeaders generateHeaders(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        headers.add(X_CM_PRODUCTTOKEN, token);
        return headers;
    }

    @Override
    public void sendStatelessSupportSms(SmsMessage smsMessage) throws SmsException {
        String[] numbers = configuration.getHotlineNumbers().split(configuration.getApplicationSeparator());
        for (String number : numbers) {
            if (!number.isEmpty()) {
                SmsMessage smsToSupport = new SmsMessage();
                smsToSupport.setCirrusRef(smsMessage.getCirrusRef());
                smsToSupport.setSenderId(smsMessage.getSenderId());
                smsToSupport.setPhone(number);
                smsToSupport.setMessage(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")) + " " + smsMessage.getSenderId() + " : " + smsMessage.getMessage());
                this.sendSmsWithouDataSave(smsToSupport);
            }
        }
    }

    private void sendSmsWithouDataSave(SmsMessage smsMessage) throws SmsException {
        //Les services appelants valorisent sendDate, MAIS ils ne donnent pas la valeur en UTC.
        //Pour éviter de modifier tous nos MS, je repasse dessus (le sendDate dans les services appelant est valorisé avec LocalDateTime.now())
        smsMessage.setSendDate(LocalDateTime.now(ZoneOffset.UTC));
        smsMessage.setType("SMS");
        smsMessage.setOperator(EnumOperator.CM.toString());

        HttpHeaders headers = this.generateHeaders(configuration.getCmToken());
        RestClient<CmSmsRequest> client = new RestClient<>(configuration.getCmSmsEndpoint(), headers);
        final CmSmsRequest request = this.convertMessageIntoSmsRequest(smsMessage);

        final ResponseEntity<HttpResponseBody> response = client.post(request);
        if (response != null) {
            if (!response.getStatusCode().is2xxSuccessful()) {
                throw new SmsException("CmService.sendSmsAndGetToken : Error while trying to send message to " + smsMessage.getPhone() + ". " + response.getStatusCodeValue());
            }
        }
    }


    @Override
    public void sendSupportSMS(SmsMessage smsMessage) throws SmsException {

        //check message content
        //used to throw a NullPointerException on empty smsMessage content
        if(smsMessage.getMessage() == null) {
            throw new MissingOrInvalidFieldException("message",String.class);
        }

        String[] numbers = configuration.getHotlineNumbers().split(configuration.getApplicationSeparator());
        LOGGER.debug("CmService.sendSupportSMS : support numbers : {}", numbers);
        // For the test , maxSendForSupport = 10
        int maxSendForSupport = configuration.getMaxSendForSupport();
        // recuperer le nombre de sms envoyé ce jour là

        //Pour éviter les spams, on va vérifier qu'on n'a pas envoyé exactement le même message dans les 15 minutes avant maintenant.
        //ce qui engendre un comportement bien différent de la vérification faite auparavant par numberOfSmsForSupportNumber
        //Par exemple, on ne devrait plus saturer le quota, et on peut ainsi continuer à recevoir les messages légitimes.
        if (!isSmsSentDuringLastMinutes(smsMessage, numbers)) {
            int index = smsMessageRepository.numberOfSmsForSupportNumber(numbers);
            LOGGER.debug("Nombre de sms envoyé à la date du {} : {}", LocalDateTime.now(), index);
            //Strictement inférieur car l'indice commence à 0
            if (index < maxSendForSupport) {
                for (String number : numbers) {
                    if (!number.isEmpty()) {
                        SmsMessage smsToSupport = new SmsMessage();
                        smsToSupport.setCirrusRef(smsMessage.getCirrusRef());
                        smsToSupport.setSenderId(smsMessage.getSenderId());
                        smsToSupport.setPhone(number);
                        smsToSupport.setMessage(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")) + " " + smsMessage.getSenderId() + " : " + smsMessage.getMessage());
                        LOGGER.debug("CmService.sendSupportSMS - envoi du message {}", smsToSupport);
                        this.sendSmsAndGetToken(smsToSupport);
                    }
                }
            } else {
                LOGGER.error("le nombre maximal d'envoi du message a été depassé");
                // le count est pour incrémenter au moment où on envoi un message vocal, et on a fixé le nombre d'envoi pour ne pas avoir de spam,
                count++;
                if (count <= configuration.getMaxSendVocalsms()) {
                    for (String number : numbers) {
                        if (!number.isEmpty()) {
                            SmsMessage message = new SmsMessage();
                            message.setSendDate(smsMessage.getSendDate());
                            message.setMessage(configuration.getMessage());
                            message.setSenderId(configuration.getOnCallDutyNumber());
                            this.sendVocalMessage(message);
                        }
                    }
                } else {
                    LOGGER.error("le nombre maximal d'appel vocal a été depassé : {}", count);
                }
            }
        } else {
            LOGGER.info("EsendexService.sendSupportSMS : SMS non envoyé (texte : {}, date : {} ). isSmsSentDuringLastMinutes == true", smsMessage.getMessage(), smsMessage.getSendDate());
        }
    }


    /**
     * Récupère en base de donnéesla valeur à l'instant T du statut du sms identifié par son token (côté cm.com)
     *
     * @param token
     * @return
     * @throws IOException
     * @throws SmsException
     * @throws JAXBException
     */
    @Override
    public SmsMessage getSmsStatus(String token) throws SmsException {
        return smsMessageRepository.findSmsByToken(token);
    }


    /**
     * Tout comme pour le statut, récupérer la réponse d'un sms revient à lire le champ incomingSms dudit sms, donc on retourne celui qui nous intéresse.
     */
    @Override
    public SmsMessage getSmsResponse(String token, String phoneNumber) throws
            BadResponseFromEsendex, IOException, UnableToGetContent, ParseException {
        return smsMessageRepository.findSmsByToken(token);
    }


    /**
     * Pas de SDK disponible pour la voix ! implémentation manuelle donc...
     * endpoint : https://api.cm.com/voiceapi/v2/Notification
     * <pre>
     * exemple de message :
     * {
     * "callee": "0033XXXXXXX",
     * "caller": "0033488805401",
     * "prompt": "hello world",
     * "prompt-type": "TTS",
     * "voice": {
     * "language": "fr-FR",
     * "gender": "Female",
     * "number": 1,
     * "volume": 2
     * }
     * }
     * </pre>
     *
     * @param message
     * @return
     * @throws SmsException
     */
    @Override
    public void sendVocalMessage(SmsMessage message) throws SmsException {
        message.setType("VOCAL");
        message.setOperator(EnumOperator.CM.toString());
        smsMessageRepository.create(message);
        CmVoiceRequest voiceRequest = this.convertMessageIntoVoiceRequest(message);

        HttpHeaders headers = this.generateHeaders(configuration.getCmVoiceToken());
        RestClient<CmVoiceRequest> client = new RestClient<>(configuration.getCmVoiceEndpoint(), headers);
        client.voicePost(voiceRequest);
    }


    @Override
    public void sendMultiSMS(String[] phone, String message) throws SmsException {
        throw new SmsException("CMService.sendMultiSMS : method not yet implemented");
    }


    /**
     * Vérifie en base si le message a déjà été envoyé lors des X dernières minutes
     *
     * @param smsMessage
     * @return
     */
    private boolean isSmsSentDuringLastMinutes(SmsMessage smsMessage, String[] numbers) {
        List<SmsMessage> messages = smsMessageRepository.findRecentIdenticalSms(smsMessage, configuration.getSupportTempo(), numbers);
        //si vide => pas de sms envoyés, donc retour false pour répondre à la question isSmsSentDuringLastMinutes
        LOGGER.debug("Cmservice.isSmsSentDuringLastMinutes : answer to request : {}", !messages.isEmpty());
        return !messages.isEmpty();
    }


    @Scheduled(cron = "${config.smsResetDelay}")
    // try this : 0 */3 * ? * * for the test, for preprod or prod is 0 0 0 * * ?
    public void reset() {
        count = 0;
        LOGGER.info("votre compteur a été restauré");
    }


    /**
     * Usine à gaz comparé à l'envoi de message vocaux. Je me suis inspiré de leur librairie, mais voilà le merdier.
     *
     * @param message
     * @return
     */
    private CmSmsRequest convertMessageIntoSmsRequest(SmsMessage message) throws SmsException {
        return convertMessageIntoSmsRequestWithFromI18N(message, "FR");
    }


    //Construction d'un message en prenant en compte le numéro de téléphone "FROM" en fonction du pays dans lequel on envoie (pour qu'à l'étranger ils puissent répondre sur ce téléphone)
    private CmSmsRequest convertMessageIntoSmsRequestWithFromI18N(SmsMessage message, String countryCode)  {

        if (message == null) {
            throw new IllegalArgumentException("message mustn't be null");
        }
        CmSmsRequest request = new CmSmsRequest();

        //Authentification :
        Authentication auth = new Authentication(configuration.getCmToken());
        request.setAuthentication(auth);
        Recipient recipient = new Recipient();

        //TODO : je pense que dans la base on a d'autres cas qui ne sont pas de type +33670XXX ou 33670XXX
        if (message.getPhone() != null) {
            String phone = message.getPhone().replace("+", "");
            if (!phone.startsWith(PHONE_PREFIX)) {
                phone = PHONE_PREFIX + phone.trim();
            }
            recipient.setNumber(phone);
            Message message1 = new Message(new Body(message.getMessage(), "auto"), configuration.getCmFrom(countryCode), Arrays.asList(recipient));
            message1.setAllowedChannels(new Channel[]{Channel.SMS});
            message1.setMinimumNumberOfMessageParts(1);
            message1.setMaximumNumberOfMessageParts(8);
            if (message.getId() != null) {
                message1.setReference(message.getId().toString());
            }
            request.setMessagesDetail(new Message[]{message1});
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("CmService.convertMessageIntoSmsRequestWithFromI18N : valeur de la requête : {}", request);
            }
            return request;
        }
        LOGGER.info("message.getPhone() is null. message value : " + message.toString());
        throw new MissingOrInvalidFieldException("phone",String.class);
    }


    /**
     * Conversion du message générique en message CM.com.
     *
     * @param message
     * @return
     */
    private CmVoiceRequest convertMessageIntoVoiceRequest(SmsMessage message) {
        CmVoiceRequest voiceRequest = new CmVoiceRequest();
        voiceRequest.setCallee(message.getPhone());
        voiceRequest.setCaller(configuration.getCmVoiceFrom());
        voiceRequest.setPromptType("TTS");
        voiceRequest.setPrompt(message.getMessage());

        CmVoiceRequest.Voice voice = new CmVoiceRequest.Voice();
        voice.setGender("Female");
        voice.setLanguage("fr-FR");
        voice.setNumber(1);
        voice.setVolume(2);
        voiceRequest.setVoice(voice);

        return voiceRequest;
    }

}
