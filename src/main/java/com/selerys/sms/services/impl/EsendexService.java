package com.selerys.sms.services.impl;

import com.selerys.sms.bean.esendex.MessageBody;
import com.selerys.sms.bean.esendex.MessageHeader;
import com.selerys.sms.bean.esendex.MessageHeaders;
import com.selerys.sms.configuration.AppConfiguration;
import com.selerys.sms.enumeration.EnumOperator;
import com.selerys.sms.exception.BadResponseFromEsendex;
import com.selerys.sms.exception.MissingOrInvalidFieldException;
import com.selerys.sms.exception.SmsException;
import com.selerys.sms.exception.UnableToGetContent;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.repository.SmsMessageRepository;
import com.selerys.sms.services.MarshallingService;
import com.selerys.sms.services.SmsService;
import esendex.sdk.java.ServiceFactory;
import esendex.sdk.java.model.domain.request.SmsMessageRequest;
import esendex.sdk.java.model.domain.request.VoiceMessageRequest;
import esendex.sdk.java.model.domain.response.MessageResultResponse;
import esendex.sdk.java.model.domain.response.ResourceLinkResponse;
import esendex.sdk.java.model.types.CharacterSet;
import esendex.sdk.java.model.types.MessageLanguage;
import esendex.sdk.java.service.BasicServiceFactory;
import esendex.sdk.java.service.MessagingService;
import esendex.sdk.java.service.auth.UserPassword;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;


/**
 * Fournit les méthodes d'envoi de sms
 */
@Service
public class EsendexService implements SmsService {

    private static final Logger LOGGER = LogManager.getLogger();

    private AppConfiguration appConfiguration;

    private SmsMessageRepository smsMessageRepository;

    private MarshallingService marshallingService;

    private MessagingService messagingService;

    static Integer count = 0;


    @Autowired
    public EsendexService(AppConfiguration appConfiguration, SmsMessageRepository smsMessageRepository, MarshallingService marshallingService) {
        this.appConfiguration = appConfiguration;
        this.smsMessageRepository = smsMessageRepository;
        this.marshallingService = marshallingService;
    }

    /**
     * Permet d'envoyer et de récuperer le token de la réponse du message envoyé
     *
     * @return
     */
    @Override
    public String sendSmsAndGetToken(SmsMessage smsMessage) throws SmsException {
        LOGGER.info("EsendexService.sendSmsAndGetToken : {} - envoi du sms {} au numéro {} ", smsMessage.getSendDate(), smsMessage.getMessage(), smsMessage.getPhone());
        smsMessage.setSendDate(LocalDateTime.now(ZoneOffset.UTC));
        smsMessage.setType("SMS");
        smsMessage.setOperator(EnumOperator.ESENDEX.toString());
        String result = "";
        MessageResultResponse response = this.sendEsendexAndGetResponse(smsMessage);
        if (response != null) {
            List<ResourceLinkResponse> messageIds = response.getMessageIds();
            if (!messageIds.isEmpty()) {
                result = messageIds.get(0).getId();
            }
        }
        return result;
    }


    @Override
    public void sendStatelessSupportSms(SmsMessage smsMessage) throws SmsException {
        String[] numbers = appConfiguration.getHotlineNumbers().split(appConfiguration.getApplicationSeparator());
        for (String number : numbers) {
            if (!number.isEmpty()) {
                SmsMessage smsToSupport = new SmsMessage();
                smsToSupport.setCirrusRef(smsMessage.getCirrusRef());
                smsToSupport.setSenderId(smsMessage.getSenderId());
                smsToSupport.setPhone(number);
                smsMessage.setSendDate(LocalDateTime.now(ZoneOffset.UTC));
                smsMessage.setType("SMS");
                smsMessage.setOperator(EnumOperator.ESENDEX.toString());
                smsToSupport.setMessage(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")) + " " + smsMessage.getSenderId() + " : " + smsMessage.getMessage());
                this.sendSmsWithouDataSave(smsToSupport);
            }
        }
    }


    private void sendSmsWithouDataSave(SmsMessage smsMessage) throws SmsException {
        this.connect();
        try {
            SmsMessageRequest esendexMessage = new SmsMessageRequest(smsMessage.getPhone(), smsMessage.getMessage(), CharacterSet.GSM);
            messagingService.sendMessage(appConfiguration.getEsendexAccount(), esendexMessage);
        } catch (Exception e) {
            LOGGER.error("sendEsendexAndGetResponse : Erreur lors de l'envoie de SMS : {}", smsMessage.getMessage());
            LOGGER.error(e);
            throw new SmsException("sendEsendexAndGetResponse : Erreur lors de l'envoie de SMS", e);
        }
    }


    /**
     * Même fonction que sendSms mais retourne la réponse de l'envoi (permet entre autre de récupérer l'id du sms).
     * Du coup, suppression de sendSms qui n'a plus aucun intérêt.
     *
     * @param message
     * @return
     */
    private MessageResultResponse sendEsendexAndGetResponse(SmsMessage message) throws SmsException {
        connect();
        try {
            // création d'un compteur d'envoi de mail par numéro de telephone
            message.setType("SMS");
            message.setOperator(EnumOperator.ESENDEX.toString());
            final SmsMessage smsMessage = smsMessageRepository.create(message);
            SmsMessageRequest esendexMessage = new SmsMessageRequest(message.getPhone(), message.getMessage(), CharacterSet.GSM);

            final MessageResultResponse response = messagingService.sendMessage(appConfiguration.getEsendexAccount(), esendexMessage);
            if (response != null && response.getMessageIds() != null && !response.getMessageIds().isEmpty()) {
                smsMessageRepository.updateSmsToken(smsMessage, response.getMessageIds().get(0).getId());
            }
            return response;
        } catch (Exception e) {
            LOGGER.error("sendEsendexAndGetResponse : Erreur lors de l'envoie de SMS : {}", message.getMessage());
            LOGGER.error(e);
            throw new SmsException();
        }
    }


    /**
     * Envoi d'un message à tous les numéros de supports configurés dans application.properties (asynchrone).
     */
    public void sendSupportSMS(SmsMessage smsMessage) throws SmsException {

        if(smsMessage.getMessage() == null) {
            throw new MissingOrInvalidFieldException("invalid or missing message content");
        }

        String[] numbers = appConfiguration.getHotlineNumbers().split(appConfiguration.getApplicationSeparator());
        // For the test , maxSendForSupport = 10
        int maxSendForSupport = appConfiguration.getMaxSendForSupport();
        // recuperer le nombre de sms envoyé ce jour là

        //Pour éviter les spams, on va vérifier qu'on n'a pas envoyé exactement le même message dans les 15 minutes avant maintenant.
        //ce qui engendre un comportement bien différent de la vérification faite auparavant par numberOfSmsForSupportNumber
        //Par exemple, on ne devrait plus saturer le quota, et on peut ainsi continuer à recevoir les messages légitimes.

        if (!isSmsSentDuringLastMinutes(smsMessage, numbers)) {
            int index = smsMessageRepository.numberOfSmsForSupportNumber(numbers);
            LOGGER.debug("Nombre de sms envoyé à la date du {} : {}", LocalDateTime.now(), index);
            //Strictement inférieur car l'indice commence à 0
            if (index < maxSendForSupport) {
                for (String number : numbers) {
                    if (!number.isEmpty()) {
                        SmsMessage smsToSupport = new SmsMessage();
                        smsToSupport.setCirrusRef(smsMessage.getCirrusRef());
                        smsToSupport.setSenderId(smsMessage.getSenderId());
                        smsToSupport.setPhone(number);
                        smsToSupport.setMessage(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")) + " " + smsMessage.getSenderId() + " : " + smsMessage.getMessage());
                        this.sendSmsAndGetToken(smsToSupport);
                    }
                }
            } else {
                LOGGER.error("le nombre maximal d'envoi du message a été depassé");
                // le count est pour incrémenter au moment où on envoi un message vocal, et on a fixé le nombre d'envoi pour ne pas avoir de spam,
                count++;
                if (count <= appConfiguration.getMaxSendVocalsms()) {
                    for (String number : numbers) {
                        if (!number.isEmpty()) {
                            SmsMessage message = new SmsMessage();
                            message.setSendDate(smsMessage.getSendDate());
                            message.setMessage(appConfiguration.getMessage());
                            message.setSenderId(appConfiguration.getOnCallDutyNumber());
                            this.sendVocalMessage(message);
                        }
                    }
                } else {
                    LOGGER.error("le nombre maximal d'appel vocal a été depassé : {}", count);
                }
            }
        } else {
            LOGGER.info("EsendexService.sendSupportSMS : SMS non envoyé (texte : {}, date : {} ). isSmsSentDuringLastMinutes == true", smsMessage.getMessage(), smsMessage.getSendDate());
        }
    }


    /**
     * Méthode qui vérifie en base si le message a déja été envoyé lors des X dernières minutes
     *
     * @param smsMessage
     * @return
     */
    private boolean isSmsSentDuringLastMinutes(SmsMessage smsMessage, String[] numbers) {
        List<SmsMessage> messages = smsMessageRepository.findRecentIdenticalSms(smsMessage, appConfiguration.getSupportTempo(), numbers);
        //si vide => pas de sms envoyés, donc retour false pour répondre à la question isSmsSentDuringLastMinutes
        return !messages.isEmpty();
    }


    @Scheduled(cron = "${config.smsResetDelay}")
    // try this : 0 */3 * ? * * for the test, for preprod or prod is 0 0 0 * * ?
    public void reset() {
        count = 0;
        LOGGER.info("votre compteur a été restauré");
    }

    /**
     * Envoi d'un message vocal
     *
     * @param message
     * @return
     * @throws SmsException
     */
    public void sendVocalMessage(SmsMessage message) throws SmsException {
        try {
            connect();
            message.setOperator(EnumOperator.ESENDEX.toString());
            message.setType("VOCAL");
            final SmsMessage smsMessage = smsMessageRepository.create(message);
            VoiceMessageRequest voiceMessageRequest = new VoiceMessageRequest(message.getPhone(), message.getMessage());
            voiceMessageRequest.setMessageLanguage(MessageLanguage.FR_FR);
            voiceMessageRequest.setFrom(appConfiguration.getFrom());
            final MessageResultResponse response = messagingService.sendMessage(appConfiguration.getEsendexAccount(), voiceMessageRequest);
            smsMessageRepository.updateSmsToken(smsMessage, response.getMessageIds().get(0).getId());
        } catch (Exception e) {
            LOGGER.error("Can not send on call duty sms", e);
            throw new SmsException(e.getMessage());
        }
    }


    /**
     * Permet de récupérer les messages de retour dans la boite conversation entre Radar/Routeur et esendex
     * ces sms sont filtrer par le status du message et la date d'envoi
     *
     * @param phoneNumber
     * @return
     * @throws BadResponseFromEsendex
     * @throws IOException
     * @throws UnableToGetContent
     * @throws ParseException
     */
    public MessageHeaders getRecentMessageHeaderForRadar(String phoneNumber) throws BadResponseFromEsendex, IOException, UnableToGetContent {
        String requestPhoneNumber = phoneNumber.replace("+", "");
        return this.getConversation(requestPhoneNumber);
    }


    /**
     * Recuperer la conversation entre le sobli/radar et esendex
     * doc : The Conversation resource can be used to retrieve the most recent 15 messages for a specified telephone number
     *
     * @param phoneNumber
     * @return les 15 derniers messages sur un numéro donné.
     * @throws IOException
     * @throws BadResponseFromEsendex
     */
    public MessageHeaders getConversation(String phoneNumber) throws IOException, BadResponseFromEsendex, UnableToGetContent {
        String esendexLogin = appConfiguration.getEsendexId();
        String esendexPwd = appConfiguration.getEsendexPwd();

        //we are not using this serviceFactory further in the code
        BasicServiceFactory serviceFactory = ServiceFactory.createBasicAuthenticatingFactory(new UserPassword(esendexLogin, esendexPwd));
        String url = appConfiguration.getEsendexConversationUri() + phoneNumber + appConfiguration.getEsendexConversationEndPoint();
        HttpResponse response = this.executeEsendexRequest(url);
        if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            return marshallingService.unmarshalMessageHeaders(result);
        } else {
            throw new BadResponseFromEsendex(" bad response " + response.getStatusLine().getStatusCode() + "request url:" + url);
        }

    }

    /**
     * Cette methode permet de verifier la longueur de la date de retour du sobli action puis recuperer le bon format de cette date
     * Il arrive que la date de retour du message contient 24 ou 23 caracteres, si c'est le cas on utilise le pattern avec sss ou ss
     *
     * @param date date de retour du message
     * @return
     */
    public DateTimeFormatter patternFormat(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        if (date.length() == 24) {
            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        } else if (date.length() == 23) {
            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SS'Z'");
        } else if (date.length() == 22) {
            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
        }
        return formatter;
    }


    /**
     * Cette methode permet de verifier la longueur de la date de retour du sobli action puis recuperer le bon format de cette date
     * Il arrive que la date de retour du message contient 24 ou 23 caracteres, si c'est le cas on utilise le pattern avec sss ou ss
     *
     * @param date date de retour du message
     * @return
     */
    public SimpleDateFormat patternSimpleDateFormat(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        if (date.length() == 24) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        } else if (date.length() == 23) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'");
        } else if (date.length() == 22) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
        }
        return dateFormat;
    }

    /**
     * cette methode recupére le body des messages header en passant par url
     *
     * @param esendexId
     * @return
     * @throws IOException
     * @throws BadResponseFromEsendex
     * @throws UnableToGetContent
     */
    public com.selerys.sms.bean.esendex.MessageBody getBodyText(String esendexId) throws IOException, BadResponseFromEsendex, UnableToGetContent {
        String url = appConfiguration.getEsendexMessageHeadersUri() + esendexId + appConfiguration.getEsendexMessageHeadersEndPoint();
        HttpResponse response = this.executeEsendexRequest(url);
        if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            return marshallingService.unmarshalMessageBody(result);
        } else {
            throw new BadResponseFromEsendex(" bad response " + response.getStatusLine().getStatusCode() + "request url:" + url);
        }
    }


    /***
     * cette méthode permet de retourner le statut d'un  sms qui est retrouvé grâce au token en parametre
     * @param token

     * @return
     */
    public SmsMessage getSmsStatus(String token) throws SmsException, IOException {
        SmsMessage status = new SmsMessage();

        String url = appConfiguration.getEsendexMessageHeadersUri() + token;
        HttpResponse response = this.executeEsendexRequest(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            try {
                MessageHeader messageHeader = marshallingService.unmarshalMessageHeader(result);
                status.setStatus(messageHeader.getStatus());

                DateTimeFormatter formatter = this.patternFormat(messageHeader.getSentat());
                LocalDateTime dateSent = LocalDateTime.parse(messageHeader.getSentat(), formatter);
                status.setSendDate(dateSent);

                DateTimeFormatter formatterStatus = this.patternFormat(messageHeader.getLaststatusat());
                LocalDateTime dateStatus = LocalDateTime.parse(messageHeader.getLaststatusat(), formatterStatus);
                status.setStatusDate(dateStatus);
                status.setSmsId(token);
            } catch (UnableToGetContent e) {
                throw new SmsException("EsendexService.getSmsStatus : error " + e.getMessage());
            }
            return status;
        } else {
            throw new SmsException(" bad response " + response.getStatusLine().getStatusCode() + "request url:" + url);
        }
    }

    private HttpResponse executeEsendexRequest(String url) throws IOException {
        String esendexLogin = appConfiguration.getEsendexId();
        String esendexPwd = appConfiguration.getEsendexPwd();

        HttpGet request = new HttpGet((url));
        String auth = esendexLogin + ":" + esendexPwd;
        byte[] encodedAuth = org.apache.commons.codec.binary.Base64.encodeBase64(
                auth.getBytes(StandardCharsets.ISO_8859_1));
        //Build the header string "basic
        String authHeader = "Basic " + new String(encodedAuth);
        //set the created header string asactual header in your request
        request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
        HttpClient client = HttpClientBuilder.create().build();
         return client.execute(request);
    }


    /**
     * Particularité pour esendex, sur la demande d'une réponse, il faut aller interroger esendex à ce moment là.
     * Ce n'est pas comme pour CM ou on reçoit en mode push les messages sur un endpoint
     *
     * @param phoneNumber
     * @return
     * @throws BadResponseFromEsendex
     * @throws IOException
     * @throws UnableToGetContent
     * @throws ParseException
     */
    @Override
    public SmsMessage getSmsResponse(String smsToken, String phoneNumber) throws BadResponseFromEsendex, IOException, UnableToGetContent, ParseException {
        final MessageHeaders response = this.getRecentMessageHeaderForRadar(phoneNumber);
        String requestPhoneNumber = phoneNumber.replace("+", "");
        final List<MessageHeader> messageHeaders = response.getMessageHeaderList();

        messageHeaders.sort(Comparator.comparing(MessageHeader::getSentat).reversed());

        for (MessageHeader messageHeader : response.getMessageHeaderList()) {
            //Attention, le From provient de celui qui fait la réponse, pas du from qui a envoyé le premier SMS.
            if (messageHeader.getFrom().getPhoneNumber().equals(requestPhoneNumber)) {
                final SmsMessage smsMessage = this.smsMessageRepository.findSmsByToken(smsToken);

                if (messageHeader.getSentat() != null) {
                    DateTimeFormatter formatter = this.patternFormat(messageHeader.getSentat());
                    LocalDateTime sentDate = LocalDateTime.parse(messageHeader.getSentat(), formatter);
                    smsMessage.setIncomingSmsDate(sentDate);
                }

                if (messageHeader.getLaststatusat() != null) {
                    DateTimeFormatter formatterStatus = this.patternFormat(messageHeader.getLaststatusat());
                    LocalDateTime dateStatus = LocalDateTime.parse(messageHeader.getLaststatusat(), formatterStatus);
                    smsMessage.setStatusDate(dateStatus);
                }
                smsMessage.setStatus(messageHeader.getStatus());

                String id = messageHeader.getBody().getId();
                final MessageBody responseBody = this.getBodyText(id);
                String returnMessage = responseBody.getBodytext();
                smsMessage.setIncomingSms(returnMessage);

                smsMessage.setSmsId(smsToken);
                smsMessageRepository.updateSms(smsMessage);
                return smsMessage;
            }
        }
        return null;
    }


    public void sendMultiSMS(String[] phone, String message) throws SmsException {
        //TODO : à implémenter si est nécessaire.
    }


    private void connect() {
        String esendexLogin = appConfiguration.getEsendexId();
        String esendexPwd = appConfiguration.getEsendexPwd();

        BasicServiceFactory serviceFactory = ServiceFactory.createBasicAuthenticatingFactory(new UserPassword(esendexLogin, esendexPwd));
        messagingService = serviceFactory.getMessagingService();
    }

}