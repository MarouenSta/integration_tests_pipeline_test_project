package com.selerys.sms.services.impl;

import com.selerys.sms.bean.cm.push.SmsPush;
import com.selerys.sms.bean.cm.push.StatusPush;
import com.selerys.sms.bean.launch.sequence.LaunchDeclarationBuilder;
import com.selerys.sms.bean.launch.sequence.LaunchDeclarationMessage;
import com.selerys.sms.configuration.AppConfiguration;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.repository.SmsMessageRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class CmPushService {

    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private SmsMessageRepository messageRepository;

    @Autowired
    private AppConfiguration configuration;

    private RestTemplate restTemplate = new RestTemplate();


    /**
     * Enregistrement en base de données du statut d'un sms.
     * La doc cm.com indique qu'ils ne sont pas garants de l'ordre dans lequel ils vont nous envoyer les status.
     * Ce qui veut dire que si on garde l'ancienne manière de faire, on risque d'écraser la dernière valeure par une valeur plus ancienne.
     * Je prends donc le parti de modifier notre fonctionnement : création d'une nouvelle table qui va enregistrer la totalité des messages.
     *
     * @param status
     */
    public void savePushedStatus(StatusPush status) {
        //on récupère d'abord le sms concerné, pour savoir si son statut est bel et bien antérieur au statut que l'on vient de recevoir.
        if (status != null && status.getMsg() != null) {
            final String reference = status.getMsg().getReference();
            if (reference != null && !reference.trim().isEmpty()) {
                final SmsMessage smsToUpdate = messageRepository.findSmsById(Integer.valueOf(status.getMsg().getReference()));
                if (smsToUpdate != null) {
                    Integer newStatus = status.getMsg().getStatus().getCode();
                    Integer oldStatus = smsToUpdate.getStatusCode();

                    if (oldStatus == null || newStatus.compareTo(oldStatus) > 0) {
                        messageRepository.updateStatusFromCmPush(status);
                    } else {
                        LOGGER.info("Received status for sms {} is not an update.", status.getMsg().getReference());
                    }
                } else {
                    LOGGER.info("no sms find with reference : {}", status.getMsg().getReference());
                }
            } else {
                LOGGER.info("Received reference is null or empty");
            }
        } else {
            LOGGER.info("Received status or message is null");
        }
    }


    /**
     * Regle CM : pour recevoir une réponse à un sms, il faut mettre comme numéro expéditeur le 38082.
     *
     * @param message
     */
    public void savePushedSms(SmsPush message) {
        //We try to parse the message to see if it's a message to follow sobli launch sequence
        LaunchDeclarationBuilder builder = new LaunchDeclarationBuilder(message.getMessage().getText());
        LaunchDeclarationMessage launchDeclarationMessage = builder.build();
        if (launchDeclarationMessage != null) {
            final String url = configuration.getCirrusUrl() + "/ms/sobli/launch/automatic?access_token={token}";
            restTemplate.postForEntity(url, launchDeclarationMessage, Boolean.class, configuration.getToken());
        } else {
            //Else it's just an anwser, we just need to register the message.
            messageRepository.updateSmsWithResponse(message);
        }
    }

}