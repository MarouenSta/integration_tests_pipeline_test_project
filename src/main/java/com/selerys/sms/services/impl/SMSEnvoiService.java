package com.selerys.sms.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.selerys.sms.bean.smsenvoi.SendSmsRequest;
import com.selerys.sms.bean.smsenvoi.SendSmsResponse;
import com.selerys.sms.bean.smsenvoi.StatusSmsResponse;
import com.selerys.sms.configuration.AppConfiguration;
import com.selerys.sms.exception.BadResponseFromEsendex;
import com.selerys.sms.exception.SmsException;
import com.selerys.sms.exception.UnableToGetContent;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.repository.SmsMessageRepository;
import com.selerys.sms.services.SmsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

/**
 * Implémentation des méthodes pour le futur second prestataire
 */
@Service
public class SMSEnvoiService implements SmsService {

    public static final String BASEURL = "https://api.smsenvoi.com/API/v1.0/REST/";
    private static final Logger LOGGER = LogManager.getLogger();
    public static final String APPLICATION_JSON = "application/json";

    private SmsMessageRepository smsMessageRepository;

    private AppConfiguration config;

    private ObjectMapper mapper;

    @Autowired
    public SMSEnvoiService(AppConfiguration config, SmsMessageRepository smsMessageRepository) {
        this.smsMessageRepository = smsMessageRepository;
        this.config = config;
        this.mapper = new ObjectMapper();
        this.mapper.setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE);
    }


    @Override
    public void sendSupportSMS(SmsMessage smsMessage) throws SmsException {
        String[] numbers = config.getHotlineNumbers().split(config.getApplicationSeparator());
        for (String number : numbers) {
            if (!number.isEmpty()) {
                SmsMessage smsToSupport = new SmsMessage();
                smsToSupport.setPhone(number);
                smsToSupport.setCirrusRef(smsMessage.getCirrusRef());
                smsToSupport.setSenderId(smsMessage.getSenderId());
                smsToSupport.setMessage(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")) + " " + smsMessage.getSenderId() + " : " + smsMessage.getMessage());
                this.sendSmsAndGetToken(smsToSupport);
            } else {
                throw new SmsException("SMSEnvoiService.sendSupportSMS : number is empty");

            }
        }
    }


    @Override
    public String sendSmsAndGetToken(SmsMessage message) throws SmsException {
        message.setSendDate(LocalDateTime.now(ZoneOffset.UTC));
        message.setType("SMS");
        message.setOperator("SMS_ENVOI");
        final SmsMessage smsMessage = smsMessageRepository.create(message);
        try {
            String[] authKeys = this.login(config.getSmsenvoiLogin(), config.getSmsenvoiPassword());
            SendSmsRequest sendSMS = new SendSmsRequest();
            sendSMS.setMessage(message.getMessage());
            sendSMS.addRecipient(message.getPhone());
            sendSMS.setSender("Selerys");
            final SendSmsResponse sendSmsResponse = this.sendSMS(authKeys, sendSMS);
            smsMessageRepository.updateSmsToken(smsMessage, sendSmsResponse.getOrderId());
            return sendSmsResponse.getOrderId();
        } catch (Exception e) {
            throw new SmsException("SMSEnvoiService.sendSmsAndGetToken error " + e.getMessage());
        }
    }


    @Override
    public void sendStatelessSupportSms(SmsMessage smsMessage) throws SmsException {
        String[] numbers = config.getHotlineNumbers().split(config.getApplicationSeparator());
        for (String number : numbers) {
            if (!number.isEmpty()) {
                try {
                    String[] authKeys = this.login(config.getSmsenvoiLogin(), config.getSmsenvoiPassword());
                    SendSmsRequest sendSMS = new SendSmsRequest();
                    sendSMS.setMessage(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")) + " " + smsMessage.getSenderId() + " : " + smsMessage.getMessage());
                    sendSMS.addRecipient(number);
                    sendSMS.setSender("Selerys");
                    this.sendSMS(authKeys, sendSMS);
                } catch (Exception e) {
                    throw new SmsException("SMSEnvoiService.sendSmsAndGetToken error " + e.getMessage());
                }
            }
        }
    }


    /**
     * @param message
     * @return
     * @throws SmsException
     */
    @Override
    public void sendVocalMessage(SmsMessage message) throws SmsException {
        // SMSenvoi ne propose pas les appels vocaux.
        // On se contente d'envoyer un sms classique "de secours";
        this.sendSmsAndGetToken(message);
    }


    @Override
    public SmsMessage getSmsStatus(String token) throws IOException {

        String[] authKeys = this.login(config.getSmsenvoiLogin(), config.getSmsenvoiPassword());
        URL url = new URL(BASEURL + "sms/" + token);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setRequestProperty("user_key", authKeys[0]);
            conn.setRequestProperty("Session_key", authKeys[1]);
            conn.setRequestProperty("Accept", APPLICATION_JSON);
            conn.setRequestProperty("Content-type", APPLICATION_JSON);
            conn.setRequestMethod("GET");

            if (conn.getResponseCode() != 200) {
                this.writeErrorInLogAndThrowException(conn);
            }
            StringBuilder response = this.getResponseFromConn(conn);

            final StatusSmsResponse statusSmsResponse = mapper.readValue(response.toString(), StatusSmsResponse.class);
            //TODO : convertir le status en status générique de chez nous. Pour l'instant, on a DLVRD et WAIT4DLVR
            final String status = statusSmsResponse.getRecipients().get(0).getStatus();

            //update du sms en base
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            LocalDateTime statusDate = null;
            if (!statusSmsResponse.getRecipients().isEmpty() && statusSmsResponse.getRecipients().get(0) != null &&
                    (statusSmsResponse.getRecipients().get(0).getDeliveryDate() != null && !statusSmsResponse.getRecipients().get(0).getDeliveryDate().isEmpty())) {
                statusDate = LocalDateTime.parse(statusSmsResponse.getRecipients().get(0).getDeliveryDate(), formatter);
            }
            smsMessageRepository.updateStatus(token, status, statusDate);
            return smsMessageRepository.findSmsByToken(token);
        } finally {
            conn.disconnect();
        }
    }


    @Override
    public SmsMessage getSmsResponse(String smsToken, String phoneNumber) throws BadResponseFromEsendex, IOException, UnableToGetContent, ParseException {

        //TODO : implémentation à faire si cela est possible.

        return null;
    }


    /**
     * Authenticates the user given it's username and password.
     * Returns the pair user_key, Session_key
     *
     * @param username The user username
     * @param password The user password
     * @return A list with 2 strings. Index 0 is the user_key, index 1 is the Session_key
     * @throws IOException If an error occurs
     */
    private String[] login(String username, String password) throws IOException {

        URL url = new URL(BASEURL + "login");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        String encoding = Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));
        conn.setRequestProperty("Authorization", "Basic " + encoding);
        conn.setRequestMethod("GET");

        if (conn.getResponseCode() != 200) {
            this.writeErrorInLogAndThrowException(conn);
        }
        StringBuilder response = this.getResponseFromConn(conn);
        conn.disconnect();
        return response.toString().split(";");
    }


    /**
     * Sends an SMS message
     *
     * @param authKeys The pair of user_key and Session_key
     * @param sendSMS  The SendSMS object
     * @throws IOException If an error occurs
     */
    private SendSmsResponse sendSMS(String[] authKeys, SendSmsRequest sendSMS) throws SmsException {
        HttpURLConnection conn = null;
        try {
            URL url = new URL("https://api.smsenvoi.com/API/v1.0/REST/sms");

            conn = (HttpURLConnection) url.openConnection();

            // Sending an SMS requires authentication
            conn.setRequestProperty("user_key", authKeys[0]);
            conn.setRequestProperty("Session_key", authKeys[1]);
            conn.setRequestProperty("Accept", APPLICATION_JSON);
            conn.setRequestProperty("Content-type", APPLICATION_JSON);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            String payload = mapper.writeValueAsString(sendSMS);

            //Le stream ci-dessous est fermé dans le disconnect du finally.
            OutputStream os = conn.getOutputStream();
            os.write(payload.getBytes());
            os.flush();

            if (conn.getResponseCode() != 201) {
                this.writeErrorInLogAndThrowException(conn);
            }

            StringBuilder response = this.getResponseFromConn(conn);
            return mapper.readValue(response.toString(), SendSmsResponse.class);

        } catch (Exception e) {
            LOGGER.error(e);
            throw new SmsException(e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    private StringBuilder getResponseFromConn(HttpURLConnection conn) throws IOException {
        StringBuilder response = new StringBuilder();
        try (InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
             BufferedReader br = new BufferedReader(inputStreamReader)) {
            String output;
            while ((output = br.readLine()) != null) {
                response.append(output);
            }
        }
        return response;
    }


    private void writeErrorInLogAndThrowException(HttpURLConnection conn) throws IOException {
        StringBuilder error = new StringBuilder();
        String output;
        try (InputStreamReader inputStreamReader = new InputStreamReader(conn.getErrorStream(), StandardCharsets.UTF_8);
             BufferedReader errorbuffer = new BufferedReader(inputStreamReader)) {
            while ((output = errorbuffer.readLine()) != null) {
                error.append(output);
            }
        }
        if (LOGGER.isErrorEnabled()) {
            LOGGER.error(error.toString());
        }
        throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
    }


    @Override
    public void sendMultiSMS(String[] phone, String message) throws SmsException {
        LOGGER.info("Method not implemented");
    }

}