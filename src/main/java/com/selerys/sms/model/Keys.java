/*
 * This file is generated by jOOQ.
 */
package com.selerys.sms.model;


import com.selerys.sms.model.tables.SmsMessage;
import com.selerys.sms.model.tables.records.SmsMessageRecord;

import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.Internal;


/**
 * A class modelling foreign key relationships and constraints of tables in 
 * sms.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<SmsMessageRecord> SMS_MESSAGE_PKEY = Internal.createUniqueKey(SmsMessage.SMS_MESSAGE, DSL.name("sms_message_pkey"), new TableField[] { SmsMessage.SMS_MESSAGE.ID }, true);
}
