package com.selerys.sms.bean.smsenvoi;


/**
 * Accueille le retour d'un envoi de sms pour SMS ENVOI
 * Type de message :
 * <pre>{
 *     "result" : "OK",                    "//OK or errors"
 *     "order_id" : "123456789",
 *     "total_sent" : 2                    "//SMS sent or credits used"
 * }</pre>
 */
public class SendSmsResponse {

    private String result;
    private String orderId;
    private int totalSent;
    private int remainingCredits;
    private String internalOrderId;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getTotalSent() {
        return totalSent;
    }

    public void setTotalSent(int totalSent) {
        this.totalSent = totalSent;
    }

    public int getRemainingCredits() {
        return remainingCredits;
    }

    public void setRemainingCredits(int remainingCredits) {
        this.remainingCredits = remainingCredits;
    }

    public String getInternalOrderId() {
        return internalOrderId;
    }

    public void setInternalOrderId(String internalOrderId) {
        this.internalOrderId = internalOrderId;
    }
    public boolean isValid() {
        return "OK".equals(result);
    }
}