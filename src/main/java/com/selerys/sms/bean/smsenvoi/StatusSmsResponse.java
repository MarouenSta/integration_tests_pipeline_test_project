package com.selerys.sms.bean.smsenvoi;


import java.util.List;

/**
 * Pour accueillir la réponse à un getStatus de SMSEnvoi
 * <pre>{
 *     "recipient_number": 1,
 *     "result": "OK",
 *     "recipients": [
 *         {
 *             "status": "WAITING",
 *             "destination": "+333471234567",
 *             "delivery_date": "20180307175609",
 *             "first_click_date": "yyyyMMddHHmmss","// (Optional, present for Rich SMS) When the embedded link was clicked first"
 *             "last_click_date": "yyyyMMddHHmmss", "// (Optional, present for Rich SMS) When the embedded link was clicked last"
 *             "total_clicks": "561"                "// (Optional, present for Rich SMS) Total number of clicks on the link"
 *         }
 *     ]
 * }</pre>
 */
public class StatusSmsResponse {

    private int recipientNumber;

    private String result;
    private List<Recipient> recipients;


    public StatusSmsResponse() {
        //Constructeur par défaut.
    }

    public int getRecipientNumber() {
        return recipientNumber;
    }

    public void setRecipientNumber(int recipientNumber) {
        this.recipientNumber = recipientNumber;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<Recipient> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<Recipient> recipients) {
        this.recipients = recipients;
    }

    public static class Recipient {
        private String status;
        private String destination;
        private String deliveryDate;
        private String message;

        public Recipient() {
            //Constructeur par défaut.
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
