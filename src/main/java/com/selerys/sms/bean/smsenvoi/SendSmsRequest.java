package com.selerys.sms.bean.smsenvoi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Classe donnée par la documentation :
 * This object is used to create an SMS message sending request.
 */
public class SendSmsRequest {
    /**
     * The message body
     */
    private String message;

    /**
     * The message type
     */
    private String messageType = "PRM";

    /**
     * Should the API return the remaining credits?
     */
    private boolean returnCredits = false;

    /**
     * The list of recipients
     */
    private List<String> recipient = new ArrayList<>();

    /**
     * The sender Alias (TPOA)
     */
    private String sender = null;

    /**
     * Postpone the SMS message sending to the specified date
     */
    private Date scheduledDeliveryTime = null;

    /**
     *
     */
    private String orderId;

    private String internalOrderId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public boolean isReturnCredits() {
        return returnCredits;
    }

    public void setReturnCredits(boolean returnCredits) {
        this.returnCredits = returnCredits;
    }

    public List<String> getRecipient() {
        return recipient;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getScheduledDeliveryTime() {
        return scheduledDeliveryTime;
    }

    public void setScheduledDeliveryTime(Date scheduledDeliveryTime) {
        this.scheduledDeliveryTime = scheduledDeliveryTime;
    }

    public void addRecipient(String recipient) {
        this.recipient.add(recipient);
    }

    public void setRecipient(List<String> recipient) {
        this.recipient = recipient;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getInternalOrderId() {
        return internalOrderId;
    }

    public void setInternalOrderId(String internalOrderId) {
        this.internalOrderId = internalOrderId;
    }
}