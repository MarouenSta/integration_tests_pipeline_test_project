package com.selerys.sms.bean.cm.push;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("status")
public class StatusDetail {

    /**
     * Code du statut
     * 0 = accepted by the operator
     * 1 = rejected by CM or operator
     * 2 = delivered
     * 3 = failed (message was not and will not be delivered)
     * 4 = Read (Not for SMS, only other channels)
     */
    private int code;

    /**
     * Code de l'erreur si le code statut est un code erreur (failed je suppose)
     */
    private String errorCode;

    //Apparemment errorDescription n'est pas la description d'une erreur mais du status tout court
    private String errorDescription;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }


    @Override
    public String toString() {
        return "StatusDetail{" +
                "code=" + code +
                ", errorCode='" + errorCode + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                '}';
    }
}
