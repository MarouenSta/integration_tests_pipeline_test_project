package com.selerys.sms.bean.cm;

import java.util.Arrays;

public class HttpResponseBody {

        private String details ;
        private int errorCode ;
        private ResponseMessageDetail[] messages ;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public int getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(int errorCode) {
            this.errorCode = errorCode;
        }

        public ResponseMessageDetail[] getMessages() {
            return messages;
        }

        public void setMessages(ResponseMessageDetail[] messages) {
            this.messages = messages;
        }


    @Override
    public String toString() {
        return "HttpResponseBody{" +
                "details='" + details + '\'' +
                ", errorCode=" + errorCode +
                ", messages=" + Arrays.toString(messages) +
                '}';
    }
}
