package com.selerys.sms.bean.cm.push;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("msg")
public class StatusNotification {

    private String received;
    private String to;
    private String reference;
    private StatusDetail status;

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public StatusDetail getStatus() {
        return status;
    }

    public void setStatus(StatusDetail status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "StatusNotification{" +
                "received='" + received + '\'' +
                ", to='" + to + '\'' +
                ", reference='" + reference + '\'' +
                ", status=" + status.toString() +
                '}';
    }
}