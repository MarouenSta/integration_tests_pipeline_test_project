package com.selerys.sms.bean.cm;


/**
 * Exemple de Structure du message pour passer un appel voix via CM.com:
 * {
 * "callee": "00316xxxxxxxx",
 * "caller": "003176xxxxxxx",
 * "prompt": "This is a test notification call using the C M voice A P I.",
 * "prompt-type": "TTS",
 * "voice": {
 * "language": "fr-FR",
 * "gender": "Female",
 * "number": 1,
 * "volume": 2
 * }
 * "voicemail-response": "Ignore",
 * "max-replays": 2,
 * "auto-replay": false,
 * "replay-prompt": "Press 1 to repeat this message."
 * }
 */
public class CmVoiceRequest {

    private String callee;
    private String caller;
    private String prompt;
    private String promptType;
    private String voiceMailResponse;
    private String maxReplay;
    private String autoReplay;
    private String replayPrompt;
    private Voice voice;

    public static class Voice {

        private String language;
        private String gender;
        private Integer number;
        private Integer volume;

        public Voice() {
            //commentaire pour faire plaisir à sonar
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }

        public Integer getVolume() {
            return volume;
        }

        public void setVolume(Integer volume) {
            this.volume = volume;
        }
    }

    public String getCallee() {
        return callee;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getPromptType() {
        return promptType;
    }

    public void setPromptType(String promptType) {
        this.promptType = promptType;
    }

    public String getVoiceMailResponse() {
        return voiceMailResponse;
    }

    public void setVoiceMailResponse(String voiceMailResponse) {
        this.voiceMailResponse = voiceMailResponse;
    }

    public String getMaxReplay() {
        return maxReplay;
    }

    public void setMaxReplay(String maxReplay) {
        this.maxReplay = maxReplay;
    }

    public String getAutoReplay() {
        return autoReplay;
    }

    public void setAutoReplay(String autoReplay) {
        this.autoReplay = autoReplay;
    }

    public String getReplayPrompt() {
        return replayPrompt;
    }

    public void setReplayPrompt(String replayPrompt) {
        this.replayPrompt = replayPrompt;
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }
}
