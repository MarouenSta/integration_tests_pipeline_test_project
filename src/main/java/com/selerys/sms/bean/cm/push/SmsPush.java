package com.selerys.sms.bean.cm.push;


import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Structure JSON d'un objet SMS reçu par cm.com qu'ils nous font suivre en mode PUSH.
 * {
 * "from": {
 * "number": "+316012345678",
 * "name": ""
 * },
 * "to": {
 * "number": "3669"
 * },
 * "message": {
 * "text": "This is an example message",
 * "media": {
 * "mediaUri": "",
 * "contentType": "",
 * "title": ""
 * },
 * "custom": {}
 * },
 * "reference": "2f2d42ac-3809-40fb-bce5-dc720e400000",
 * "groupings": [
 * "39373ce0-f4aa-4918-8ff1-3cef7f77b112",
 * "messagesApi",
 * ""
 * ],
 * "timeUtc": "2019-11-05T08:32:33",
 * "channel": "WhatsApp"
 * }
 */
public class SmsPush {

    private From from;
    private To to;
    private Message message;
    private String reference;
    private String[] groupings;
    private Timestamp timeUtc;
    private String channel;

    public SmsPush() {
    }

    class From {
        private String number;
        private String name;

        public From() {
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "From{" +
                    "number='" + number + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }


    class To {

        private String number;

        public To() {
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        @Override
        public String toString() {
            return "To{" +
                    "number='" + number + '\'' +
                    '}';
        }
    }


    public class Message {

        private String text;
        private Media media;
        private Custom custom;

        public Message() {
        }

        class Media {
            private String mediaUri;
            private String contentType;
            private String title;

            public Media() {
            }

            public String getMediaUri() {
                return mediaUri;
            }

            public void setMediaUri(String mediaUri) {
                this.mediaUri = mediaUri;
            }

            public String getContentType() {
                return contentType;
            }

            public void setContentType(String contentType) {
                this.contentType = contentType;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            @Override
            public String toString() {
                return "Media{" +
                        "mediaUri='" + mediaUri + '\'' +
                        ", contentType='" + contentType + '\'' +
                        ", title='" + title + '\'' +
                        '}';
            }
        }

        class Custom {
            public Custom() {
            }
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Media getMedia() {
            return media;
        }

        public void setMedia(Media media) {
            this.media = media;
        }

        public Custom getCustom() {
            return custom;
        }

        public void setCustom(Custom custom) {
            this.custom = custom;
        }

        @Override
        public String toString() {
            return "Message{" +
                    "text='" + text + '\'' +
                    ", media=" + media +
                    ", custom=" + custom +
                    '}';
        }
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String[] getGroupings() {
        return groupings;
    }

    public void setGroupings(String[] groupings) {
        this.groupings = groupings;
    }

    public Timestamp getTimeUtc() {
        return timeUtc;
    }

    public void setTimeUtc(Timestamp timeUtc) {
        this.timeUtc = timeUtc;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        return "SmsPush{" +
                "from=" + from +
                ", to=" + to +
                ", message=" + message +
                ", reference='" + reference + '\'' +
                ", groupings=" + Arrays.toString(groupings) +
                ", timeUtc=" + timeUtc +
                ", channel='" + channel + '\'' +
                '}';
    }
}
