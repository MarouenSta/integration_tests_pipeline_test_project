package com.selerys.sms.bean.cm.push;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Message pusher par cm.com vers notre MS
 * <p>
 * {
 * "messages": {
 * "msg": {
 * "received": "[CREATED_S]",
 * "to": "[GSM]",
 * "reference": "[REFERENCE]",
 * "status": {
 * "code": "[STATUS]",
 * "errorCode": "[STATUSDESCRIPTION]",
 * "errorDescription": "[STANDARDERRORTEXT]"
 * },
 * "operator": "[OPERATOR]"
 * }
 * }
 * }
 */

@JsonTypeName("messages")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class StatusPush {

    @JsonProperty("msg")
    private StatusNotification msg;

    public StatusNotification getMsg() {
        return msg;
    }

    public void setMsg(StatusNotification msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "StatusPush{" +
                "msg=" + msg.toString() +
                '}';
    }
}