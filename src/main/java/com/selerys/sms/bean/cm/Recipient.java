package com.selerys.sms.bean.cm;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Recipient {

    /// <summary>
    ///     This value should be in international format.
    ///     A single mobile number per request. Example: '00447911123456'
    /// </summary>
    @JsonProperty("number")
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Recipient{" +
                "number='" + number + '\'' +
                '}';
    }

}