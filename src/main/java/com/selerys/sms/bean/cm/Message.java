package com.selerys.sms.bean.cm;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;

public class Message {
    /// <summary>
    ///     The allowed channels field forces a message to only use certain routes.
    ///     In this field you can define a list of which channels you want your message to use.
    ///     Not defining any channels will be interpreted as allowing all channels.
    /// </summary>
    /// <remarks>
    ///     Note that for channels other than SMS, CM needs to configure the out going flows.
    ///     For those flows to work, we need to be contacted.
    /// </remarks>
    @JsonProperty("allowedChannels")
    private Channel[] allowedChannels;

    /// <summary>
    ///     Required: The actual text body of the message.
    /// </summary>
    @JsonProperty("body")
    private Body body;

    /// <summary>
    ///     The custom grouping field is an optional field that can be used to tag messages.
    ///     These tags are be used by CM products, like the Transactions API.
    ///     Applying custom grouping names to messages helps filter your messages.
    ///     With up to three levels of custom grouping fields that can be set, subsets of messages can be
    ///     further broken down. The custom grouping name can be up to 100 characters of your choosing.
    ///     It’s recommended to limit the number of unique custom groupings to 1000.
    ///     Please contact support in case you would like to exceed this number.
    /// </summary>
    @JsonProperty("customGrouping")
    private String customGrouping = "text-sdk-java";

    /// <summary>
    ///     The custom grouping2 field, like <see cref="CustomGrouping" /> is an optional field that can be used to tag
    ///     messages.
    ///     These tags are be used by CM products, like the Transactions API.
    ///     Applying custom grouping names to messages helps filter your messages.
    ///     With up to three levels of custom grouping fields that can be set, subsets of messages can be
    ///     further broken down. The custom grouping name can be up to 100 characters of your choosing.
    ///     It’s recommended to limit the number of unique custom groupings to 1000.
    ///     Please contact support in case you would like to exceed this number.
    /// </summary>
    @JsonProperty("customGrouping2")
    private String customGrouping2;

    /// <summary>
    ///     The custom grouping3 field, like <see cref="CustomGrouping" /> and <see cref="CustomGrouping2" /> is an optional
    ///     field that can be used to tag messages.
    ///     These tags are be used by CM products, like the Transactions API.
    ///     Applying custom grouping names to messages helps filter your messages.
    ///     With up to three levels of custom grouping fields that can be set, subsets of messages can be
    ///     further broken down. The custom grouping name can be up to 100 characters of your choosing.
    ///     It’s recommended to limit the number of unique custom groupings to 1000.
    ///     Please contact support in case you would like to exceed this number.
    /// </summary>
    /// <remarks>Default value within this SDK is <see cref="Common.Constant.TextSdkReference" /></remarks>
    @JsonProperty("customGrouping3")
    private String customGrouping3;

    /// <summary>
    ///     Required: This is the sender name.
    ///     The maximum length is 11 alphanumerical characters or 16 digits. Example: 'CM Telecom'
    /// </summary>
    @JsonProperty("from")
    private String from;


    /// <summary>
    ///     Used when sending multipart or concatenated SMS messages and always used together with
    ///     <see cref="MinimumNumberOfMessageParts" />.
    ///     Indicate the minimum and maximum of message parts that you allow the gateway to send for this
    ///     message.
    ///     Technically the gateway will first check if a message is larger than 160 characters, if so, the
    ///     message will be cut into multiple 153 characters parts limited by these parameters.
    /// </summary>
    @JsonProperty("maximumNumberOfMessageParts")
    private Integer maximumNumberOfMessageParts;

    /// <summary>
    ///     Used when sending multipart or concatenated SMS messages and always used together with
    ///     <see cref="MaximumNumberOfMessageParts" />.
    ///     Indicate the minimum and maximum of message parts that you allow the gateway to send for this
    ///     message.
    ///     Technically the gateway will first check if a message is larger than 160 characters, if so, the
    ///     message will be cut into multiple 153 characters parts limited by these parameters.
    /// </summary>
    @JsonProperty("minimumNumberOfMessageParts")
    private Integer minimumNumberOfMessageParts;

    /// <summary>
    ///     Required: The destination mobile numbers.
    ///     This value should be in international format.
    ///     A single mobile number per request. Example: '00447911123456'
    /// </summary>
    @JsonProperty("to")
    private List<Recipient> recipients;

    /// <summary>
    ///     Optional: For each message you send, you can set a reference.
    ///     The given reference will be used in the status reports and MO replies for the message,
    ///     so you can link the messages to the sent batch.
    ///     For more information on status reports, see:
    ///     https://docs.cmtelecom.com/business-messaging/v1.0#/status_report_webhook
    ///     The given reference must be between 1 - 32 alphanumeric characters, and will not work using demo accounts.
    /// </summary>
    @JsonProperty("reference")
    private String reference;


    /// <summary>
    ///     Optional: For each message you send, you can set a validity.
    ///     Specify a time at which a delayed message can be considered irrelevant, you can supply an absolute date & time 
    ///     or a relative offset. A message is considered failed if it was not successfully delivered before that time. 
    ///     And via a Status Report we inform you this was the case.
    ///     For more information on status reports, see:
    ///     https://docs.cmtelecom.com/business-messaging/v1.0#/status_report_webhook
    ///     You can supply the time zone for the validity period using either of the following formats:
    ///     
    ///     Absolute date and time:
    ///     
    ///     2017-04-20 11:50:05 GMT
    ///     2017-04-20 11:50:05+8
    ///     2017-04-20 11:55:05-07:00
    ///     If no time zone was specified, CE(S)T will be used. (CM local time)
    ///     
    ///     Relative offset in hour(H) or minute(M)
    ///     
    ///     2h
    ///     30m
    ///     You can set the validity in either hours or minutes. A combination of both is not supported.
    /// </summary>
    @JsonProperty("validity")
    private String validity;


    public Message(Body body, String from, List<Recipient> recipientList) {
        this.body = body;
        this.from = from;
        this.recipients = recipientList;
    }


    public Channel[] getAllowedChannels() {
        return allowedChannels;
    }

    public void setAllowedChannels(Channel[] allowedChannels) {
        this.allowedChannels = allowedChannels;
    }

    public com.selerys.sms.bean.cm.Body getBody() {
        return body;
    }

    public void setBody(com.selerys.sms.bean.cm.Body body) {
        this.body = body;
    }

    public String getCustomGrouping() {
        return customGrouping;
    }

    public void setCustomGrouping(String customGrouping) {
        this.customGrouping = customGrouping;
    }

    public String getCustomGrouping2() {
        return customGrouping2;
    }

    public void setCustomGrouping2(String customGrouping2) {
        this.customGrouping2 = customGrouping2;
    }

    public String getCustomGrouping3() {
        return customGrouping3;
    }

    public void setCustomGrouping3(String customGrouping3) {
        this.customGrouping3 = customGrouping3;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Integer getMaximumNumberOfMessageParts() {
        return maximumNumberOfMessageParts;
    }

    public void setMaximumNumberOfMessageParts(Integer maximumNumberOfMessageParts) {
        this.maximumNumberOfMessageParts = maximumNumberOfMessageParts;
    }

    public Integer getMinimumNumberOfMessageParts() {
        return minimumNumberOfMessageParts;
    }

    public void setMinimumNumberOfMessageParts(Integer minimumNumberOfMessageParts) {
        this.minimumNumberOfMessageParts = minimumNumberOfMessageParts;
    }

    public List<Recipient> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<Recipient> recipients) {
        this.recipients = recipients;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    @Override
    public String toString() {
        return "Message{" +
                "allowedChannels=" + Arrays.toString(allowedChannels) +
                ", body=" + body +
                ", customGrouping='" + customGrouping + '\'' +
                ", customGrouping2='" + customGrouping2 + '\'' +
                ", customGrouping3='" + customGrouping3 + '\'' +
                ", from='" + from + '\'' +
                ", maximumNumberOfMessageParts=" + maximumNumberOfMessageParts +
                ", minimumNumberOfMessageParts=" + minimumNumberOfMessageParts +
                ", recipients=" + recipients +
                ", reference='" + reference + '\'' +
                ", validity='" + validity + '\'' +
                '}';
    }
}
