package com.selerys.sms.bean.cm;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Authentication {

    @JsonProperty("producttoken")
    private String productToken;

    public String getProductToken() {
        return this.productToken;
    }

    public void setProductToken(String productToken) {
        this.productToken = productToken;
    }

    public Authentication(String productToken) {
        this.productToken = productToken;
    }

    @Override
    public String toString() {
        return "Authentication{" +
                "productToken='" + productToken + '\'' +
                '}';
    }

}