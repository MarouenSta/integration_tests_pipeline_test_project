package com.selerys.sms.bean.cm;

public class ResponseMessageDetail {

    private String messageDetails ;
    private String messageErrorCode;
    private int parts;
    private String reference;
    private String status;
    private String to;


    public String getMessageDetails() {
        return messageDetails;
    }

    public void setMessageDetails(String messageDetails) {
        this.messageDetails = messageDetails;
    }

    public String getMessageErrorCode() {
        return messageErrorCode;
    }

    public void setMessageErrorCode(String messageErrorCode) {
        this.messageErrorCode = messageErrorCode;
    }

    public int getParts() {
        return parts;
    }

    public void setParts(int parts) {
        this.parts = parts;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "ResponseMessageDetail{" +
                "messageDetails='" + messageDetails + '\'' +
                ", messageErrorCode='" + messageErrorCode + '\'' +
                ", parts=" + parts +
                ", reference='" + reference + '\'' +
                ", status='" + status + '\'' +
                ", to='" + to + '\'' +
                '}';
    }
}