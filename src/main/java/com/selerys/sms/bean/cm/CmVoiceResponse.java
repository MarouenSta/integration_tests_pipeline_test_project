package com.selerys.sms.bean.cm;

/**
 * Exemple de réponse de l'API CM.com lors de l'envoi d'un message Voix.
 * {  "type": "notification-finished",
 * "call-id": "6d0f176e-00ac-4de2-a04d-b3f7c895adb0",
 * "instruction-id": "aeba8cae-eadd-4b49-9350-8061893b3d66",
 * "caller": "003176xxxxxxx",
 * "callee": "00316xxxxxxxx",
 * "result": {
 * "code": 10,
 * "description": "Finished"
 * },
 * "started-on": "2018-02-09T11:00:31.752Z",
 * "answered-on": "2018-02-09T11:00:37.972Z",
 * "finished-on": "2018-02-09T11:00:48.112Z"
 * }
 */
public class CmVoiceResponse {

    private String type;
    private String callId;
    private String instructionId;
    private String caller;
    private String callee;
    private Result result;
    private String startedOn;
    private String answeredOn;
    private String finishedOn;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public String getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(String instructionId) {
        this.instructionId = instructionId;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getCallee() {
        return callee;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(String startedOn) {
        this.startedOn = startedOn;
    }

    public String getAnsweredOn() {
        return answeredOn;
    }

    public void setAnsweredOn(String answeredOn) {
        this.answeredOn = answeredOn;
    }

    public String getFinishedOn() {
        return finishedOn;
    }

    public void setFinishedOn(String finishedOn) {
        this.finishedOn = finishedOn;
    }

    private class Result {
        private int code;
        private String description;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
