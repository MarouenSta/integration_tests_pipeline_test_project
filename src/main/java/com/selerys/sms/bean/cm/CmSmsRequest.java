package com.selerys.sms.bean.cm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Arrays;

/**
 * Message au format cm.com qui permet d'envoyer un sms
 */
@JsonTypeName("messages")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class CmSmsRequest {

    @JsonProperty("authentication")
    private Authentication authentication;

    @JsonProperty("msg")
    private Message[] messagesDetail;


    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public Message[] getMessagesDetail() {
        return this.messagesDetail;
    }

    public void setMessagesDetail(Message[] messagesDetail) {
        this.messagesDetail = messagesDetail;
    }

    public CmSmsRequest() {
        //constructeur par défaut.
    }

    @Override
    public String toString() {
        return "CmSmsRequest{" +
                "authentication=" + authentication +
                ", messagesDetail=" + Arrays.toString(messagesDetail) +
                '}';
    }

}