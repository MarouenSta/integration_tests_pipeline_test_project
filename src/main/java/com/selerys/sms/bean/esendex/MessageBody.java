package com.selerys.sms.bean.esendex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name="messagebody")
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageBody {

    @XmlElement(name = "bodytext")
    private String bodytext;
    @XmlElement(name = "characterset")
    private String characterset;


    public String getBodytext() {
        return bodytext;
    }

    public void setBodytext(String bodytext) {
        this.bodytext = bodytext;
    }

    public String getCharacterset() {
        return characterset;
    }

    public void setCharacterset(String characterset) {
        this.characterset = characterset;
    }
}
