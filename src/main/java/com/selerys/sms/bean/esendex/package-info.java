@XmlSchema(
        namespace = "http://api.esendex.com/ns/",
        elementFormDefault = XmlNsForm.QUALIFIED)
package com.selerys.sms.bean.esendex;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;