package com.selerys.sms.bean.esendex;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Répponse d'un getStatus Esendex
 */
@XmlRootElement(name = "messageheader")
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageHeader implements Serializable {

    @XmlAttribute(name = "id")
    private String id;

    @XmlAttribute(name = "uri")
    private String uri;

    @XmlElement(name = "reference")
    private String reference;

    @XmlElement(name = "status")
    private String status;

    @XmlElement(name = "deliveredat")
    private String deliveredat;


    @XmlElement(name = "sentat")
    private String sentat;

    @XmlElement(name = "laststatusat")
    private String laststatusat;

    @XmlElement(name = "submittedat")
    private String submittedat;

    @XmlElement(name = "type")
    private String type;

    @XmlElement(name = "to")
    private To to;

    @XmlElement(name = "from")
    private From from;

    @XmlElement(name = "summary")
    private String summary;

    @XmlElement(name = "body")
    private EsendexBody body;

    @XmlElement(name = "direction")
    private String direction;

    @XmlElement(name = "parts")
    private String parts;

    @XmlElement(name = "username")
    private String username;

    @XmlElement(name = "batch")
    private Batch batch;

    public EsendexBody getBody() {
        return body;
    }

    public void setBody(EsendexBody body) {
        this.body = body;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLaststatusat() {
        return laststatusat;
    }

    public void setLaststatusat(String laststatusat) {
        this.laststatusat = laststatusat;
    }

    public String getSubmittedat() {
        return submittedat;
    }

    public void setSubmittedat(String submittedat) {
        this.submittedat = submittedat;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDeliveredat() {
        return deliveredat;
    }

    public void setDeliveredat(String deliveredat) {
        this.deliveredat = deliveredat;
    }

    public String getSentat() {
        return sentat;
    }

    public void setSentat(String sentat) {
        this.sentat = sentat;
    }

    public MessageHeader() {
    }

    public MessageHeader(String id, To to, From from) {
        this.id = id;
        this.to = to;
        this.from = from;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getParts() {
        return parts;
    }

    public void setParts(String parts) {
        this.parts = parts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Batch getBatch() {
        return batch;
    }

    public void setBatch(Batch batch) {
        this.batch = batch;
    }

}
