package com.selerys.sms.bean.esendex;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "messageheaders")
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageHeaders {

    @XmlAttribute(name = "startindex")
    private Integer startindex;

    @XmlAttribute(name = "count")
    private Integer count;

    @XmlAttribute(name = "totalcount")
    private Integer totalcount;

    @XmlElement(name = "messageheader")
    private List<MessageHeader> messageHeaderList;


    public Integer getStartindex() {
        return startindex;
    }

    public void setStartindex(Integer startindex) {
        this.startindex = startindex;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(Integer totalcount) {
        this.totalcount = totalcount;
    }

    public List<MessageHeader> getMessageHeaderList() {
        return messageHeaderList;
    }

    public void setMessageHeaderList(List<MessageHeader> messageHeaderList) {
        this.messageHeaderList = messageHeaderList;
    }
}
