package com.selerys.sms.bean.ms;

import java.util.HashMap;
import java.util.Map;

public class TestSmsStatus {

    private boolean success;

    private Map<String, Boolean> detail;

    public TestSmsStatus() {
        this.detail= new HashMap<>();
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Map<String, Boolean> getDetail() {
        return detail;
    }

    public void setDetail(Map<String, Boolean> detail) {
        this.detail = detail;
    }
}
