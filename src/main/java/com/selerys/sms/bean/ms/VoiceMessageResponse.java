package com.selerys.sms.bean.ms;

import com.selerys.sms.bean.cm.CmVoiceResponse;

public class VoiceMessageResponse {

    private String id;
    private CmVoiceResponse status;
    private String code;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CmVoiceResponse getStatus() {
        return status;
    }

    public void setStatus(CmVoiceResponse status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
