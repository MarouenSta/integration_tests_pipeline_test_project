package com.selerys.sms.bean;

public class ExceptionResponse {
    private String errorMessage;
    private String requestedURI;
    private String statusCode;
    private Long date;

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public void setRequestedURI(String requestedURI) {
        this.requestedURI = requestedURI;
    }

    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }


    public String getRequestedURI() {
        return requestedURI;
    }

    public void callerURL(final String requestedURI) {
        this.requestedURI = requestedURI;
    }

    @Override
    public String toString() {
        return "ExceptionResponse{" +
                "errorMessage='" + errorMessage + '\'' +
                ", requestedURI='" + requestedURI + '\'' +
                ", statusCode=" + statusCode +
                ", date=" + date +
                '}';
    }
}
