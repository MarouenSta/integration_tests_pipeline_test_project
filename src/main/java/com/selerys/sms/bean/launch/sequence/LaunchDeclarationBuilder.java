package com.selerys.sms.bean.launch.sequence;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LaunchDeclarationBuilder {

    private final String PATTERN = "i=\\d{1,15};s=\\d{6,10}_0;(?:n=[1-6];)?f=\\d{5,20};t=\\d{10};c=";

    private String message;

    public LaunchDeclarationBuilder(String message) {
        this.message = message;
    }

    private Boolean validate() {
        final Pattern p = Pattern.compile(PATTERN);//. represents single character
        final Matcher m = p.matcher(this.message);
        return m.find();
    }

    public LaunchDeclarationMessage build() {
        if (validate()) {
            LaunchDeclarationMessage launchDeclarationMessage = new LaunchDeclarationMessage();
            String[] splitedMessage = this.message.split(";");
            Map<String, String> attributes = new HashMap<>();
            for (int i = 0; i < splitedMessage.length; i++) {
                attributes.put(String.valueOf(splitedMessage[i].charAt(0)), splitedMessage[i].substring(2));
            }
            launchDeclarationMessage.setTransactionId(attributes.get("i"));
            launchDeclarationMessage.setContainerId(attributes.get("s").split("_")[0]);
            launchDeclarationMessage.setSender(attributes.get("f"));
            launchDeclarationMessage.setTime(Long.valueOf(attributes.get("t")) * 1000);
            launchDeclarationMessage.setCode(Integer.valueOf(attributes.get("c")));
            if (attributes.get("n") != null) {
                launchDeclarationMessage.setBalloon(Integer.valueOf(attributes.get("n")));
            }
            launchDeclarationMessage.setNotificationSource("SMS");
            return launchDeclarationMessage;
        }
        return null;
    }


}
