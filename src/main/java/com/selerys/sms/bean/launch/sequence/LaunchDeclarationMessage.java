package com.selerys.sms.bean.launch.sequence;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Objet permettant de stocker uyn message récupéré sur le MQTT pour le topic /sobli/launch
 * Exemple de structure de l'objet :
 * <p>
 * {
 * "transactionId" : "26863155426",
 * "sobliId" : "920645_0",
 * "time" : 1647333597,
 * "from" : "+33645321607",
 * "balloon" : 5,
 * "success" : false,
 * "message" : "Unable to Launch Balloon Balloon #5 In Error!: [ERR] \r\n"
 * };
 */
public class LaunchDeclarationMessage {

    @JsonProperty("sobliId")
    private String containerId;
    private String transactionId;
    @JsonProperty("from")
    private String sender;
    private Long time;
    private Integer balloon;
    private Boolean success;
    /**
     * évolution suite au lancement via l'IHM, "success" est transformé en "code"
     * Sauf qu'on ne sait pas pour l'instant si l'ancien type de message (via envoi de sms) est toujours d'actualité.
     * Donc on garde les deux champs pour des raisons de compatibilité.
     */
    private Integer code;

    private String message;

    private String notificationSource;

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getBalloon() {
        return balloon;
    }

    public void setBalloon(Integer balloon) {
        this.balloon = balloon;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LaunchDeclarationMessage that = (LaunchDeclarationMessage) o;
        return Objects.equals(containerId, that.containerId) && Objects.equals(transactionId, that.transactionId) && Objects.equals(sender, that.sender) && Objects.equals(time, that.time) && Objects.equals(balloon, that.balloon) && Objects.equals(success, that.success) && Objects.equals(code, that.code) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerId, transactionId, sender, time, balloon, success, code, message);
    }

    public String getNotificationSource() {
        return notificationSource;
    }

    public void setNotificationSource(String notificationSource) {
        this.notificationSource = notificationSource;

    }
}
