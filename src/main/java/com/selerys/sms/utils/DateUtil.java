package com.selerys.sms.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {


    private DateUtil(){}

    /**
     * Convertit une date string au format yyyy-MM-dd'T'HH:mm:ss" en LocalDateTime UTC
     * @param stringDate
     * @return
     */
    public static LocalDateTime convertStringToLocalDateTimeUTC(String stringDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime statusDate = LocalDateTime.parse(stringDate, formatter);
        ZonedDateTime ldtZoned = statusDate.atZone(ZoneId.systemDefault());
        ZonedDateTime utcZoned = ldtZoned.withZoneSameInstant(ZoneId.of("UTC"));
        return utcZoned.toLocalDateTime();
    }
}
