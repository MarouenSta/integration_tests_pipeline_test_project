package com.selerys.sms.utils;

import com.selerys.sms.configuration.AppConfiguration;
import com.selerys.sms.exception.MsHsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

/**
 * Fournit une méthode utilitaire qui retourne l'url du microservice voulu (référencé par l'enum MsUrl.
 */
@Service
public class MsUrlProvider {

    RestTemplate restTemplate;

    @Autowired
    AppConfiguration config;


    /**
     * @param msUrl
     * @return
     * @throws MsHsException
     */
    public String getActiveMsUrl(String msUrl) throws MsHsException {


        //Les adresses dans l'url sont rentrées dans l'ordre de leur importance (de qui prend le relai de qui en cas de failover).
        final String[] urls = msUrl.split(";");
        for (String url : urls) {
            //On vérifie si le service répond à cette url :
            if (isAlive(url)) {
                return url;
            }
        }
        throw new MsHsException("Aucune url disponible pour le microservice " + msUrl);
    }

    /**
     * Vérifie si le MS est OK
     *
     * @param url
     * @return
     */
    private boolean isAlive(String url) {
        String urlToCheck = url + "/actuator/health";
        try {
            this.restTemplate = buildRestTemplate();
            ResponseEntity<String> response = restTemplate.getForEntity(urlToCheck, String.class);
            return response != null && response.getBody() != null && response.getBody().equals("{\"status\":\"UP\"}");
        } catch (ResourceAccessException | HttpServerErrorException exception) {
            return false;
        }
    }

    private RestTemplate buildRestTemplate() {
        if (this.restTemplate == null) {
            return new RestTemplate(getClientHttpRequestFactory());
        } else {
            return restTemplate;
        }
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 5000;
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(timeout);
        return clientHttpRequestFactory;
    }

}