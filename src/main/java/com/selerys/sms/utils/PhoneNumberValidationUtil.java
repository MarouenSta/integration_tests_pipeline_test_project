package com.selerys.sms.utils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.springframework.stereotype.Component;

@Component
public class PhoneNumberValidationUtil {

    public boolean isPhoneOk(String phone) {

        //il faut pas laisser passé les numéros de tests
        if(phone == null || "+33600000000".equals(phone.trim()) || "33600000000".equals(phone.trim())){
            return false;
        }

        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String formattedPhone = "";

        try {
            if(phone.startsWith("00")){
                formattedPhone = "+"+phone.substring(2);
                phoneNumberUtil.parse(formattedPhone,"FR");
                return true;
            }
            // on essaye de parser un numéro de téléphone valide, avec un code pays valide.
            // Dans le cas ou le num n'est pas valide ça lève une NumberParseException
            phoneNumberUtil.parse(phone,"FR");
            return true;
        } catch (NumberParseException e) {
            return false;
        }
    }

}
