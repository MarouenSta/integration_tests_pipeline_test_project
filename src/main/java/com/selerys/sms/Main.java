package com.selerys.sms;

import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.services.SmsSentService;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.time.LocalDateTime;

/**
 * Microservice chargé de gérer l'envoi des sms : il choisit de basculer sur un autre fournisseur si esendex est KO.
 */
@SpringBootApplication(scanBasePackages = "com.selerys.*")
@EnableScheduling
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

}