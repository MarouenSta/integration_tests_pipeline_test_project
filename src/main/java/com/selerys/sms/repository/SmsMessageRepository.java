package com.selerys.sms.repository;


import com.selerys.sms.bean.cm.push.SmsPush;
import com.selerys.sms.bean.cm.push.StatusNotification;
import com.selerys.sms.bean.cm.push.StatusPush;
import com.selerys.sms.enumeration.EnumCmStatus;
import com.selerys.sms.model.tables.pojos.SmsMessage;
import com.selerys.sms.model.tables.records.SmsMessageRecord;
import com.selerys.sms.utils.DateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.*;
import java.util.Collections;
import java.util.List;

import static com.selerys.sms.model.Tables.SMS_MESSAGE;

@Repository
public class SmsMessageRepository extends AbstractJooqRespository {

    private final DSLContext dslContext;

    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    public SmsMessageRepository(DSLContext dslContext) {
        super(SmsMessageRepository.class, SMS_MESSAGE);
        this.dslContext = dslContext;
        super.dsl = dslContext;
    }

    public SmsMessage create(SmsMessage smsMessage) {
        SmsMessageRecord smsMessageRecord = dsl.newRecord(SMS_MESSAGE);
        smsMessageRecord.from(smsMessage);
        smsMessageRecord.store();
        return smsMessageRecord.into(SmsMessage.class);
    }


    /**
     * Met à jour le champ smsId qui est l'identifiant sms donné par l'opérateur.
     *
     * @param smsMessage : contient le primary key de la base de données
     * @param token      du sms retourné par l'opérateur
     */
    public void updateSmsToken(SmsMessage smsMessage, String token) {
        final SmsMessageRecord smsMessageRecord = dsl.selectFrom(SMS_MESSAGE).where(SMS_MESSAGE.ID.eq(smsMessage.getId())).fetchOne();
        smsMessageRecord.setSmsId(token);
        smsMessageRecord.update();
    }

    /**
     * Récupère le nombre de sms envoyé par jour aux numéros de support.
     *
     * @param supportNumbers : liste des numéros de support selerys.
     * @return
     */
    public Integer numberOfSmsForSupportNumber(String[] supportNumbers) {
        LocalDateTime midNightJ = LocalDateTime.now().with(LocalTime.MIDNIGHT);
        LocalDateTime midNightJPlusOne = LocalDateTime.now().with(LocalTime.MIDNIGHT).plusDays(1);
        //TODO voir  d'un point de vue métier si on souhaite limiter l'envoi à un total cumulé des téléphones, ou à un total par téléphone
        //exemple, si je mets une limite à 500, est-ce que ça veut dire que je peux recevoir 500 sms par numéros, ou alors 250 sms s'il y a deux numéros de support.

        return dsl.selectCount().from(SMS_MESSAGE)
                .where(SMS_MESSAGE.SEND_DATE.between(midNightJ, midNightJPlusOne))
                .and(SMS_MESSAGE.PHONE.in(supportNumbers))
                .fetchOne(0, Integer.class);
    }


    /**
     * méthode utilisé pour l'instant par SmsEnvoi
     *
     * @param smsId
     * @param status
     * @param statusDate
     */
    public void updateStatus(String smsId, String status, LocalDateTime statusDate) {
        final SmsMessageRecord smsMessageRecord = dsl.selectFrom(SMS_MESSAGE).where(SMS_MESSAGE.SMS_ID.eq(smsId)).fetchOne();
        smsMessageRecord.setStatus(status);
        smsMessageRecord.setStatusDate(statusDate);
        smsMessageRecord.update();
    }


    /**
     * Je voulais faire la mise à jour du status avec la mise à jour du token.
     * Mais pour éviter des impacts sur tous les prestataires, je fais une méthode à part appelée par CM uniquement pour l'instant.
     *
     * @param smsMessage
     */
    public void updateStatus(SmsMessage smsMessage) {
        dsl.update(SMS_MESSAGE)
                .set(SMS_MESSAGE.STATUS, smsMessage.getStatus())
                .set(SMS_MESSAGE.STATUS_CODE, smsMessage.getStatusCode())
                .set(SMS_MESSAGE.STATUS_ERROR_CODE, smsMessage.getStatusErrorCode())
                .set(SMS_MESSAGE.STATUS_ERROR_DESCRIPTION, smsMessage.getStatusErrorDescription())
                .where(SMS_MESSAGE.ID.eq(smsMessage.getId())).execute();
    }


    /**
     * Mise à jour d'un statut dans le cas de cm.com.
     *
     * @param status
     */
    public void updateStatusFromCmPush(StatusPush status) {
        if (status != null && status.getMsg() != null) {
            final StatusNotification msg = status.getMsg();
            LocalDateTime statusDate = DateUtil.convertStringToLocalDateTimeUTC(status.getMsg().getReceived());
            final SmsMessageRecord record = dsl.selectFrom(SMS_MESSAGE).where(SMS_MESSAGE.SMS_ID.eq(msg.getReference())).fetchOne();
            if (record != null) {

                //Bugfix : on valorise le champ status avec la valeur de l'enum. Auparavant on lui mettait ErrorDescription qui est beaucoup trop long en cas d'erreur
                //mais qui ne contient qu'un mot si tout se passe bien, d'où la méprise.
                final EnumCmStatus statusByCode = EnumCmStatus.getStatusByCode(msg.getStatus().getCode());
                record.setStatus(statusByCode.toString());
                record.setStatusCode(msg.getStatus().getCode());
                record.setStatusDate(statusDate);
                record.setStatusErrorCode(msg.getStatus().getErrorCode());
                record.setStatusErrorDescription(msg.getStatus().getErrorDescription());

                record.update();
            } else {
                LOGGER.error("SmsMessageRepository.updateStatusFromCmPush : error when retrieving smsMessage by sms_id");
            }
        }
    }


    /**
     * Récupère en base de données un sms.
     *
     * @param smsToken
     * @return
     */
    public SmsMessage findSmsByToken(String smsToken) {
        return dsl.selectFrom(SMS_MESSAGE).where(SMS_MESSAGE.SMS_ID.eq(smsToken)).fetchOneInto(SmsMessage.class);
    }


    public SmsMessage findSmsById(int smsId) {
        return dsl.selectFrom(SMS_MESSAGE).where(SMS_MESSAGE.ID.eq(smsId)).fetchOneInto(SmsMessage.class);
    }

    /**
     * Vérifie si un sms au contenu identique a été envoyé par monitoring durant les X dernières minutes définies dans la conf
     * On considère deux sms identiques s'ils ont le même contenu, même destinataire (tel cible) et même id d'expéditeur.
     *
     * @param supportTempo
     * @return
     */

    public List<SmsMessage> findRecentIdenticalSms(SmsMessage smsMessage, Integer supportTempo, String[] numbers) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRecentIdenticalSms : Recherche du message : {}", smsMessage.getMessage());
        }
        LOGGER.debug("findRecentIdenticalSms : valeur de phone : {}", smsMessage.getPhone());
        LOGGER.debug("findRecentIdenticalSms : valeur de senderId : {}", smsMessage.getSenderId());
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        LOGGER.debug("findRecentIdenticalSms : valeur de now : {}", now);
        LocalDateTime beforeNow = LocalDateTime.now(ZoneOffset.UTC).minusMinutes(supportTempo);
        LOGGER.debug("findRecentIdenticalSms : valeur de beforeNow : {}", beforeNow);
        final List<SmsMessage> smsMessages = dsl.select().from(SMS_MESSAGE)
                .where(SMS_MESSAGE.MESSAGE.like("%".concat(smsMessage.getMessage())))
                .and(SMS_MESSAGE.SENDER_ID.eq(smsMessage.getSenderId()))
                .and(SMS_MESSAGE.SEND_DATE.between(beforeNow, now))
                .and(SMS_MESSAGE.PHONE.in(numbers))
                .fetchInto(SmsMessage.class);

        if (smsMessages != null) {
            LOGGER.debug("findRecentIdenticalSms : smsMessages size : {}", smsMessages.size());
            return smsMessages;
        } else {
            LOGGER.debug("findRecentIdenticalSms : smsMessages is null");
            return Collections.emptyList();
        }

    }


    /**
     * @param message
     */
    public void updateSmsWithResponse(SmsPush message) {
        final SmsMessageRecord smsMessageRecord = dsl.selectFrom(SMS_MESSAGE).where(SMS_MESSAGE.SMS_ID.eq(message.getReference())).fetchOne();
        if (smsMessageRecord != null) {
            final SmsPush.Message message1 = message.getMessage();
            smsMessageRecord.setIncomingSms(message1.getText());
            LOGGER.info("valeur de message.getTimeUtc() : " + message.getTimeUtc());

            //Tentative désespérée
            LocalDateTime receptionDate = message.getTimeUtc().toLocalDateTime();
            ZonedDateTime ldtZoned = receptionDate.atZone(ZoneId.systemDefault());
            ZonedDateTime utcZoned = ldtZoned.withZoneSameInstant(ZoneId.of("UTC"));
            smsMessageRecord.setIncomingSmsDate(utcZoned.toLocalDateTime());

            smsMessageRecord.update();
        } else {
            LOGGER.warn("SmsMessageRepository.updateSmsWithResponse : smsMessageRecord not found with ID {}", message.getReference());
        }
    }


    /**
     * Mise à jour du message passé en argument.
     *
     * @param message
     */
    public void updateSms(SmsMessage message) {
        final SmsMessageRecord smsMessageRecord = dsl.selectFrom(SMS_MESSAGE).where(SMS_MESSAGE.ID.eq(message.getId())).fetchOne();
        smsMessageRecord.setIncomingSmsDate(message.getIncomingSmsDate());
        smsMessageRecord.setIncomingSms(message.getIncomingSms());
        smsMessageRecord.update();

    }

}