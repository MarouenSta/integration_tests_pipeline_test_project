# Copy and appricaltion for source Restore or not the database and restart the application
# $1 The environnement
# $2 RESART OR NOT THE APP - "START" for restart and "NOSTART" default value
# $3 the kind of environnement "ISOPRODUTION", "PRODUCTION" or "DEV" the default VALUE
# $4 resotre the environnement or not, "DUMP" to restore, "NODUMP" do nothing

echo "START deploy sms_ms on $1 $2 $3 $4"

# This is a watch dog to chech the variables
if [ $1 = "LOKI" ] || [ $1 = "MYSTIC" ] || [ $1 = "HULK" ]; then
  if [ $3 != "PRODUCTION" ] || [ $4 = "DUMP" ]; then
    echo "Unable to launch loki/mystic as dev/preproduction or with dump"
    exit 1
  fi
elif [ $1 = "VENOM" ] || [ $1 = "MALICIA" ] || [ $1 = "LOGAN" ]; then
  if [ $3 = "PRODUCTION" ] || [ $3 = "DEV" ]; then
    echo "Unable to launch venom/malicia as production/dev"
    exit 1
  fi
elif [ $1 = "THOR" ] || [ $1 = "STORM" ]; then
  if [ $3 = "PRODUCTION" ]; then
    echo "Unable to launch thor/storm as production"
    exit 1
  fi
fi

sudo service sms_ms stop

VERSION=$(mvn org.apache.maven.plugins:maven-help-plugin:3.1.1:evaluate -Dexpression=project.version -q -DforceStdout)
echo $VERSION

#Copy app from source
if [ $3 = "ISOPRODUCTION" ]; then
  echo "Copy on $1 isoprod"
  MV_RES=$(sudo cp /mnt/backup/deploy/cicd/temp/sms_ms/sms_ms.jar /opt/sms_ms/)
elif [ $3 = "PRODUCTION" ]; then
  echo "MEP on $1 prod"
  MV_RES=$(sudo cp /mnt/backup/deploy/cicd/mep/sms_ms/$VERSION/sms_ms.jar /opt/sms_ms)
else
  MV_RES=$(sudo mv $HOME/sms_ms.jar /opt/sms_ms/)
fi

# Check if move is ok
if [ ! -z "$MV_RES" ] && [[ -n $(echo $MV_RES | grep 'No such file or directory') ]]; then
  echo "ERROR WHEN COPYING sms_ms.jar"
  exit 1
fi

#RESTORE THE DATABASE for each deployment if it's not a MEP and only if the value is DUMP
if [ $3 != "PRODUCTION" ] && [ $4 = "DUMP" ]; then
  echo "NOTHING TO DO WITH DUMP"
fi

#Copy the application.properties in terms of the environnement
if [ $3 = "PRODUCTION" ] && [ $1 = "LOKI" ]; then
  sudo cp src/main/resources/application-prod-loki.properties /opt/sms_ms/config/application.properties
elif [ $3 = "PRODUCTION" ] && [ $1 = "MYSTIC" ]; then
  sudo cp src/main/resources/application-prod-mystic.properties /opt/sms_ms/config/application.properties
elif [ $3 = "PRODUCTION" ] && [ $1 = "HULK" ]; then
  sudo cp src/main/resources/application-prod-hulk.properties /opt/sms_ms/config/application.properties
elif [ $1 = "VENOM" ]; then
  if [ $3 = "PREPRODUCTION" ] || [ $3 = "ISOPRODUCTION" ]; then
    sudo cp src/main/resources/application-preprod-venom.properties /opt/sms_ms/config/application.properties
  fi
elif [ $1 = "MALICIA" ]; then
  if [ $3 = "PREPRODUCTION" ] || [ $3 = "ISOPRODUCTION" ]; then
    sudo cp src/main/resources/application-preprod-malicia.properties /opt/sms_ms/config/application.properties
  fi
elif [ $1 = "LOGAN" ]; then
  if [ $3 = "PREPRODUCTION" ] || [ $3 = "ISOPRODUCTION" ]; then
    sudo cp src/main/resources/application-preprod-logan.properties /opt/sms_ms/config/application.properties
  fi
elif [ $3 = "DEV" ] && [ $1 = "THOR" ]; then
  sudo cp src/main/resources/application-dev-thor.properties /opt/sms_ms/config/application.properties
elif [ $3 = "DEV" ] && [ $1 = "STORM" ]; then
  sudo cp src/main/resources/application-dev-storm.properties /opt/sms_ms/config/application.properties
else
  echo "Unable to find the good app.properties to deploy fo $3 an $1"
  exit 1
fi


#Copie du fichier de conf
if [ $3 = "PRODUCTION" ]; then
  sudo cp src/main/resources/script/sms_ms.conf /opt/scripts/etc/sms_ms.conf
fi

if [ $3 = "PREPRODUCTION" ]; then
  sudo cp src/main/resources/script/sms_ms.conf /opt/scripts/etc/sms_ms.conf
fi


#Starting app sms_ms provider
sudo chown root:root /opt/sms_ms/sms_ms.jar
sudo chmod 500 /opt/sms_ms/sms_ms.jar
sudo rm -f /opt/sms_ms/logs/*.log
if [ $2 = "START" ]; then
  echo "start the app sms_ms ! "
  STARTED=$(sudo service sms_ms start)
  # Check if start is OK
  if [ ! -z "$STARTED" ] && [[ -n $(echo $STARTED | grep Started) ]]; then
    echo "END deploy sms_ms on $1"
  else
    echo "ERROR WHEN starting sms_ms service"
    exit 1
  fi
fi