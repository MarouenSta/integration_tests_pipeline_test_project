# Script to compile, run test and check with sonar
# $1 is the environnement to save
# $2 the kind of environneemnt to save

VERSION=$(mvn org.apache.maven.plugins:maven-help-plugin:3.1.1:evaluate -Dexpression=project.version -q -DforceStdout)
echo $VERSION

# This is a watch dog to chech the variables
if [ $1 = "LOKI" ] || [ $1 = "MYSTIC" ] || [ $1 = "HULK" ]
then
    if [ $2 != "PRODUCTION" ]
    then
        echo "Unable to launch loki/mystic as dev/preproduction"
        exit 1
    fi
elif [ $1 = "VENOM" ] || [ $1 = "MALICIA" ] || [ $1 = "LOGAN" ]
then
    if [ $2 != "PREPRODUCTION" ]
    then
        echo "Unable to launch venom/malicia as production"
        exit 1
    fi
elif [ $1 = "THOR" ] || [ $1 = "STORM" ]
then
    echo "Not Allowed to copy env from thor/storm - Please compile"
    exit 1
fi

if [ $2 = "PRODUCTION" ]
then
    echo "Saving environnement $1 as production"
    sudo mkdir -p /mnt/backup/deploy/cicd/temp/sms_ms
    sudo rm -f /mnt/backup/deploy/cicd/temp/sms_ms/*
    echo "Copy to : /mnt/backup/deploy/cicd/temp/sms_ms/"
    CP_RES=$(sudo cp /opt/sms_ms/sms_ms.jar /mnt/backup/deploy/cicd/temp/sms_ms/sms_ms.jar)
elif [ $2 = "PREPRODUCTION" ]
then
    echo "Saving environnement $1 as pre-production"
    if [ -d "/mnt/backup/deploy/cicd/mep/sms_ms/$VERSION" ]
    then
        echo "Directory already exists - CAN NOT MEP THIS VERSION"
        exit 1
    else
        sudo mkdir -p /mnt/backup/deploy/cicd/mep/sms_ms/$VERSION
    fi
    echo "Copy to : /mnt/backup/deploy/cicd/mep/sms_ms/$VERSION"
    CP_RES=$(sudo cp /opt/sms_ms/sms_ms.jar /mnt/backup/deploy/cicd/mep/sms_ms/$VERSION/sms_ms.jar)
fi

# Check if move is ok
if [ ! -z "$CP_RES" ] && [[ -n $(echo $CP_RES | grep 'No such file or directory') ]]
then
    echo "ERROR WHEN COPYING sms_ms.jar"
    exit 1
fi

echo "$1 environnement succefuly saved"