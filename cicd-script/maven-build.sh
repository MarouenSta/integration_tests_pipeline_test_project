# Script to compile, run test and check with sonar
# $1 is the environnement for running - used in log only
# $2 allow to run sonar check if equals to "RUNSONAR"

echo "START BUILD $1"
MVN_RESULT=$(mvn clean install -U)
# Check if start is OK
if [ ! -z "$MVN_RESULT" ] && [[ -n $(echo $MVN_RESULT | grep 'BUILD FAILURE') ]]
then
    echo "MVN FAILURE"
    exit 1
else
    echo "MVN SUCCESS"
fi
echo "$2"
if [ $2 = 'RUNSONAR' ]
then
    mvn sonar:sonar
fi
VERSION=$(mvn org.apache.maven.plugins:maven-help-plugin:3.1.1:evaluate -Dexpression=project.version -q -DforceStdout)
echo $VERSION
cp "./target/sms_ms-$VERSION.jar" $HOME/sms_ms.jar
echo "END maven build on $1"